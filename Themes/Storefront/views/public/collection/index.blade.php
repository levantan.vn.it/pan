@extends('public.layout')

@section('content')
<div class="container">

    <div class="banner_home">
        <div class="banner_item">
            <img class="img_banner img_pc" src="assets/images/banner@2x.png" alt="Los Angeles">
            <img class="img_banner img_mobile" src="assets/images/bannermb1.png" alt="Los Angeles">
            <div class="slide_title">
                <button class="button">featured of the week</button>
                <p class="title">Love The Way You Lie</p>
                <p class="name">Eminem ft. Rihanna</p>
                <div class="icon_play"><img src="assets/images/icon/play.png" class="Play" /></div>
            </div>
        </div>
        <div class="banner_item">
            <img class="img_banner img_pc" src="assets/images/banner@2x.png" alt="Los Angeles">
            <img class="img_banner img_mobile" src="assets/images/bannermb1.png" alt="Los Angeles">
            <div class="slide_title">
                <button class="button">featured of the week</button>
                <p class="title">Love The Way You Lie</p>
                <p class="name">Eminem ft. Rihanna</p>
                <div class="icon_play"><img src="assets/images/icon/play.png" class="Play" /></div>
            </div>
        </div>
        <div class="banner_item">
            <img class="img_banner img_pc" src="assets/images/banner@2x.png" alt="Los Angeles">
            <img class="img_banner img_mobile" src="assets/images/bannermb1.png" alt="Los Angeles">
            <div class="slide_title">
                <button class="button">featured of the week</button>
                <p class="title">Love The Way You Lie</p>
                <p class="name">Eminem ft. Rihanna</p>
                <div class="icon_play"><img src="assets/images/icon/play.png" class="Play" /></div>
            </div>
        </div>

    </div>

    <div class="bg_white border_round playlist_again section_general mb-30 pl-mb-no">
        <div class="heading">
            <h2 class="title">Play Again List</h2>
            <div class="arrow">
                <div class="prev_slider1 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider1 next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider center">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Bee Bee 1</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist13.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">CTV 3</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Starboy</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist4.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Flame</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Bee Bee 7</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Bee Bee 8</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist7.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Bee Bee 3<a href="#"></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round category_shortcut_list section_general mb-30 pl-mb-no">
        <div class="heading">
            <h2 class="title">Category Shortcut List</h2>
            <div class="arrow">
                <div class="prev_slider2 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider2 next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist10.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Onerepublic</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist9.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Chaff and dust</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist8.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Do it your way</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Acidrap</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist12.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">To africa with love</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist11.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Bee Bee 12</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">To africa with love</a></h4>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round recommendation_songs section_general mb-30 pl-mb-no">
        <div class="heading">
            <h2 class="title">Recommendation Songs</h2>
            <div class="arrow">
                <div class="prev_slider3 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider3 next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist8.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Mimy</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist9.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Linking Park</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist12.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Shades Of Love</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist7.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">You</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="like">157.958 Liked</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round recommendation_collection section_general mb-30 pl-mb-no">
        <div class="heading">
            <h2 class="title">Recommendation Collection</h2>
            <div class="arrow">
                <div class="prev_slider4 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider4 next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Best Mixes</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Best day ever</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist10.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Graceland</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist11.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Shades Of Love</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist12.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Djesse</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist9.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Star Boy</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round recommendation_playlist section_general mb-30 pl-mb-no">
        <div class="heading">
            <h2 class="title">Recommendation Playlist</h2>
            <div class="arrow">
                <div class="prev_slider5 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider5 next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Soit Goes</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist8.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Amogh Symphony</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist10.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Phlearn</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist9.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Shades Of Love</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">You</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Signs of Jealousy</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round music_chart mb-30">
        <div class="heading">
            <h2 class="title">Music Chart</h2>
            <div class="arrow">
                <div class="prev_sliderchart prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_sliderchart next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="chart_list">
            <div class="chart_left">
                <div class="top_chart">
                    <div class="img">
                        <a href="#"><img src="assets/images/featureimg/playlist4.png" alt=""/></a>
                    </div>
                    <div class="info">
                        <h4 class="title">Monty Are I</h4>
                        <p>Chart Description</p>
                        <p>Last updated</p>
                        <p>157.958 Liked</p>
                    </div>
                </div>
                <div class="listbottom_chart">
                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>

                    <div class="item">
                        <div class="left">
                            <h4 class="title">Song Item</h4>
                            <p>987.654 played</p>
                        </div>
                        <div class="right">
                            <button class="play"><img src="assets/images/icon/play_small.png" alt="Play" class="hover" /><img src="assets/images/icon/play_grey.png" alt="Play" class="nohover" /></button>
                            <button class="like"><img src="assets/images/icon/like_small.png" alt="Like" class="hover" /><img src="assets/images/icon/like_grey.png" alt="Like" class="nohover" /></button>
                            <button class="share"><img src="assets/images/icon/share_small.png" alt="Share" class="hover" /><img src="assets/images/icon/share_grey.png" alt="Share" class="nohover" /></button>
                            <button class="addlist"><img src="assets/images/icon/addlist_small.png" alt="Add List" class="hover" /><img src="assets/images/icon/addlist_grey.png" alt="Add List" class="nohover" /></button>
                        </div>
                    </div>
                </div>
                <a href="#" class="see_all">See all <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="chart_right">
                <div class="list_play">
                    <div class="slider">
                        <div class="list_play_col">
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">Goldlink</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">ISYO</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                        </div>
                        <div class="list_play_col">
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist8.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">50K</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">You</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                        </div>
                        <div class="list_play_col">
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">Goldlink</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist12.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">Sia</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                        </div>
                        <div class="list_play_col">
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist10.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">Funk & Soul</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src="assets/images/featureimg/playlist9.png" alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title"><a href="#">Goldlink</a></h4>
                                <p class="author"><a href="#">Curator</a></p>
                                <p class="like">157.958 Liked</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round recommendation_curator section_general mb-30 pl-mb-no">
        <div class="heading">
            <h2 class="title">Recommendation Curator</h2>
            <div class="arrow">
                <div class="prev_slider6 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider6 next_slider arrow_slider active active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider center round">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Sia</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Martin Garrix</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Sasha Sloan</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist7.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Mariah Carey</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist9.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Selena gomez</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist11.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist8.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">50K</a></h4>
                    <p class="follow">157.958 Follows</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white border_round new_release section_general pl-mb-no">
        <div class="heading">
            <h2 class="title">New Release</h2>
            <div class="arrow">
                <div class="prev_slider7 prev_slider arrow_slider">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="next_slider7 next_slider arrow_slider active">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="list_play">
            <div class="slider">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Song Item</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Song Item</a></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Collection Item</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist4.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Song Item</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Collection Item</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Collection Item</a></h4>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist7.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Bee Bee 1<a href="#"></h4>
                    <p class="author"><a href="#">Major Lazer & DJ Snake</a></p>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
