@extends('public.layout')

@section('content')
<div class="browse">
	<div class="container">
		<div class="menu_browse radius-30 bg_white pd-30">
			<div class="heading">
      	<h2 class="title">Browse Music</h2>
      </div>
      <ul>
      	<li><a href="{{ url('/browse/category') }}">Category</a></li>
      	<li><a href="{{ url('/browse/collection') }}">Collection</a></li>
      	<li class="active"><a href="{{ url('/browse/playlist') }}">Playlist</a></li>
      	<li><a href="{{ url('/browse/curator') }}">Curator</a></li>
      	<li><a href="{{ url('/browse/chart') }}">Chart</a></li>
      	<li><a href="{{ url('/browse/new-release') }}">New Release</a></li>
      </ul>
		</div>
    <div class="browsemain">
      <div class="bg_white radius-30 browselist browselist_playlist">
            <div class="heading">
                  <h2 class="title">Playlists</h2>
                  <div class="arrow">
                        <div class="prev_playlist prev_slider arrow_slider">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="next_playlist next_slider arrow_slider active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                  </div>
            </div>
             <div class="list_play">
            <div class="list list_pc">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Onerepublic</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Chaff and dust</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Do it your way</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist4.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Flame</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">To africa with love</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Mimy</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist7.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Goldlink</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist8.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Linking Park</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist13.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Shades Of Love</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist10.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Starboy</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist11.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">You</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist12.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Soit Goes</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Amogh Symphony</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Phlearn</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist13.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Sunset</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>

            </div>
            <div class="list list_mb">
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Onerepublic</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Chaff and dust</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Do it your way</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist4.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Flame</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">To africa with love</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>
                <div class="playitem">
                    <div class="image">
                        <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                        <div class="more">
                            <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                            <div class="list_function">
                                <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                            </div>
                        </div>
                    </div>
                    <h4 class="title"><a href="#">Mimy</a></h4>
                    <p class="author"><a href="#">Curator</a></p>
                    <p class="like">157.958 Liked</p>
                </div>

            </div>
        </div>
      </div>
    </div>
	</div>
</div>
@endsection
