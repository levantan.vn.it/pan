@php
$recommmendation = [
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Djesse',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Star Boy',
        'curator' => 'Curator',
        'like' => '157.958'
    ]
];
$newrelease = [
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Djesse',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Star Boy',
        'curator' => 'Curator',
        'like' => '157.958'
    ]
    ,
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ]
    ,
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ]
    ,
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ]
];

$child = [
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Djesse',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Star Boy',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Djesse',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Star Boy',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Djesse',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Star Boy',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best Mixes',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Best day ever',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Graceland',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Djesse',
        'curator' => 'Curator',
        'like' => '157.958'
    ],
    [
        'title' => 'Star Boy',
        'curator' => 'Curator',
        'like' => '157.958'
    ]
];
@endphp
@extends('public.layout')
@section('title')
    Category Titlte
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/category.css?v='.time())}}">
@endpush
@section('content')
<div class="browse category">
    <div class="container">
        <div class="category-title">
            <h2 class="title">Category Title</h2>
        </div>

        <div class="browsemain category_playlist">
            <div class="bg_white radius-30 browselist box-shadow recommendation slide_mobile">
                <div class="heading">
                    <h2 class="title">Recommendation</h2>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($recommmendation as $song)
                        @php
                            $rand = random_int( 1, 14 );
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src={{asset("assets/images/featureimg/playlist$rand.png")}} alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$song['title']}}</a></h4>
                                <p class="author"><a href="#">{{$song['curator']}}</a></p>
                                <p class="like">{{$song['like']}} Liked</p>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>

            <div class="bg_white radius-30 browselist box-shadow most_popular slide_mobile">
                <div class="heading">
                    <h2 class="title">Most Popular</h2>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($recommmendation as $song)
                        @php
                            $rand = random_int( 1, 14 );
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src={{asset("assets/images/featureimg/playlist$rand.png")}} alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$song['title']}}</a></h4>
                                <p class="author"><a href="#">{{$song['curator']}}</a></p>
                                <p class="like">{{$song['like']}} Liked</p>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="bg_white radius-30 browselist box-shadow new_release">
                <div class="heading">
                    <h2 class="title">New Release</h2>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($newrelease as $song)
                        @php
                            $rand = random_int( 1, 14 );
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src={{asset("assets/images/featureimg/playlist$rand.png")}} alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$song['title']}}</a></h4>
                                <p class="author"><a href="#">{{$song['curator']}}</a></p>
                                <p class="like">{{$song['like']}} Liked</p>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="bg_white radius-30 browselist box-shadow child_category ">
                <div class="heading">
                    <h2 class="title">Child Categories</h2>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($child as $song)
                        @php
                            $rand = random_int( 1, 14 );
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src={{asset("assets/images/featureimg/playlist$rand.png")}} alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$song['title']}}</a></h4>
                                <p class="author"><a href="#">{{$song['curator']}}</a></p>
                                <p class="like">{{$song['like']}} Liked</p>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@push('scripts')
<script>
    if($(window).innerWidth() < 768){
        $('.recommendation .list').addClass('mobile');
        $('.most_popular .list').addClass('mobile');
    }
    $('.mobile').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 2.5,
        slidesToScroll: 2.5,
      }
    }
        ]
    });

</script>
    
@endpush