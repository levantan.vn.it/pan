<header class="header">
    <div class="container ">
        <div class="nav">
            <div class="logo">
                <a href="/"><img class="img_logo" src="assets/images/logo_pan.png" /></a>
            </div>
            <div class="menu">
                <span class="close_menu show_mobile"><i class="fa fa-times" aria-hidden="true"></i></span>
                <div class="user show_mobile">
                    <a href="#">
                        <img class="icon_user" src="assets/images/user.png" />
                        <span>이민호</span>
                    </a>
                </div>
                <ul class="menu_list">
                    <li class="menu_item"><a href="/" class="active">Home</a></li>
                    <li class="menu_item"><a href="/browse">Browse</a></li>
                    <li class="menu_item"><a href="/library">Library</a></li>
                </ul>


            </div>
            <div class="right">
                <form class="search">
                    <input type="search" placeholder="Search" />
                    <button class="icon_search"><i class="fa fa-search"></i></button>
                </form>
                <div class="user">
                    <a href="#">
                        <img class="icon_user" src="assets/images/user.png" />
                        <span>이민호</span>
                    </a>
                </div>
            </div>
            <div class="icon_menu show_mobile"><img src="assets/images/icon/icon_menu.png" alt="Menu" /></div>
        </div>
    </div>
</header>
