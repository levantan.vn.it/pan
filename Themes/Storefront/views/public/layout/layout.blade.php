<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <base href="{{ config('app.url') }}">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>
            @yield('title','Panmusic')
        </title>
        <link rel="shortcut icon" href="assets/images/panmusic-favicon.png" type="image/png">
        <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;0,900;1,400;1,700&display=swap"
            rel="preload">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" media="all">
        <link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all">
        <link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" media="all"/>
        <link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/customize.css?v='.time()) }}"  media="all">
        <link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" type="text/css" href="{{ asset('assets/css/adoniss.css?v='.time()) }}" media="all">
        @stack('meta')
        @stack('styles')
        <script>
            var base_url = "{{ url('/') }}";
        </script>

        @stack('globals')
    </head>

    <body >
        <div class="content @yield('classpage')">
            @include('layout.header')
            @yield('content')
            @include('layout.footer')
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" defer></script>
        <script src="{{ asset('assets/vendors/imagesloaded/imagesloaded.pkgd.min.js') }}" defer></script>
        <script src="{{ asset('assets/vendors/jplayer/jquery.jplayer.min.js') }}"  defer></script>
        <script src="{{ asset('assets/vendors/jplayer/jplayer.playlist.js?v=1.0') }}"  defer></script>
        <script src="{{ asset('assets/js/bootstrap-hover-menu.js') }}"  defer></script>
        <script src="{{ asset('assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"  defer></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"  defer></script>
        {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"  defer async></script> --}}
        <script src="{{ asset('assets/js/player.js?v='.time()) }}"  defer></script>
        <script src="{{ asset('assets/js/app.js') }}"  defer></script>
        <script src="{{ asset('assets/js/custom.js?v='.time()) }}"  defer></script>
        @stack('scripts')
    </body>
</html>
