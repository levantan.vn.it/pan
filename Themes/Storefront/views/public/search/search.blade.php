<?php 
 $items = [
    [
        'image'=>'assets/images/featureimg/playlist2.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Song Title',
        'subcontent'=>'SONG',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'icon'=>'assets/images/icon/like2x.png',
        'subtitle'=>'Curator Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist4.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Collections Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>'COLLECTIONS'
    ],
    [
        'image'=>'assets/images/featureimg/playlist5.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Playlist Title',
        'subcontent'=>'AUTHOR PROFILE',
        'subcontents'=>'PLAYLIST'
    ],
    [
        'image'=>'assets/images/featureimg/playlist6.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Other Profile Name',
        'subcontent'=>'PROFILE',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist7.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Category Title',
        'subcontent'=>'CATEGORY',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist8.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Chart Title',
        'subcontent'=>'CHART',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist9.png',
        'icon'=>'assets/images/icon/like2x.png',
        'subtitle'=>'Curator Name',
        'subcontent'=>'CURATOR',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist10.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Collections Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>'COLLECTIONS'
    ],
    [
        'image'=>'assets/images/featureimg/playlist11.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Collections Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>'COLLECTIONS'
    ],
];
$item2=[
    [
        'image'=>'assets/images/featureimg/playlist2.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Song Title',
        'subcontent'=>'SONG',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Collections Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist1.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Business Collections Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>''
    ]
]
?>

<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/search.css?v='.time()) }}">
</head>
@extends('public.layout')
@section('title', 'searchPage')
@section('content')
    <div class="searchPage">
        <div class="container">
            <div class="recently-searches">
                <div class="title-remove">
                    <p class="title">Recently Searches</p>
                    <img class="img-removes" src="assets/images/icon/removes.png" />
                </div>
                <div class="list">
                    @foreach ($items as $item) 
                        <div class={{$item['subtitle'] == 'Curator Title' || $item['subtitle'] == 'Curator Name'? 'item-lists':'item-list'}}>
                            <div class="list-image">
                                <div class="image-before">
                                    <img class={{$item['subtitle'] == 'Curator Title' ||$item['subtitle'] == 'Curator Name' ? 'img-curator':'img-item'}} src={{$item['image']}} />
                                </div>
                                <div class="item">
                                    <img class="img-icon" src={{$item['icon']}} />    
                                </div>
                            </div>
                            <div class="item-content">
                                <p class="subTitle">{{$item['subtitle']}}</p>
                                <p class="subContent">{{$item['subcontent']}}</p>
                                <p class="subContents">{{$item['subcontents']}}</p>
                            </div>
                            @if($item['subtitle'] !== 'Curator Title' && $item['subtitle'] !== 'Curator Name')
                            <img class="img-remove" src="assets/images/icon/remove.png" />
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="hot-collection">
                <div class="list">
                    <p class="title">Hot Collection</p>
                    @foreach ($item2 as $item) 
                        <div class="item-list">
                            <div class="list-image">
                                <div class="image-before">
                                    <img class={{$item['subtitle'] == 'Curator Title'? 'img-curator':'img-item'}} src={{$item['image']}} />
                                </div>
                                <div class="item">
                                    <img class="img-icon" src={{$item['icon']}} />    
                                </div>
                            </div>
                            <div class="item-content">
                                <p class="subTitle">{{$item['subtitle']}}</p>
                                <p class="subContent">{{$item['subcontent']}}</p>
                                <p class="subContents">{{$item['subcontents']}}</p>
                            </div>
                            @if($item['subtitle'] !== 'Curator Title')
                            <img class="img-remove" src="assets/images/icon/remove.png" />
                            @endif
                        </div>
                    @endforeach
                </div>
                <div class="list">
                    <p class="title">Top Chart</p>
                    @foreach ($item2 as $item) 
                        <div class="item-list">
                            <div class="list-image">
                                <div class="image-before">
                                    <img class={{$item['subtitle'] == 'Curator Title'? 'img-curator':'img-item'}} src={{$item['image']}} />
                                </div>
                                <div class="item">
                                    <img class="img-icon" src={{$item['icon']}} />    
                                </div>
                            </div>
                            <div class="item-content">
                                <p class="subTitle">{{$item['subtitle']}}</p>
                                <p class="subContent">{{$item['subcontent']}}</p>
                                <p class="subContents">{{$item['subcontents']}}</p>
                            </div>
                            @if($item['subtitle'] !== 'Curator Title')
                            <img class="img-remove" src="assets/images/icon/remove.png" />
                            @endif
                        </div>
                    @endforeach
                </div>
                <div class="list">
                    <p class="title">New Release</p>
                    @foreach ($item2 as $item) 
                        <div class="item-list">
                            <div class="list-image">
                                <div class="image-before">
                                    <img class={{$item['subtitle'] == 'Curator Title'? 'img-curator':'img-item'}} src={{$item['image']}} />
                                </div>
                                <div class="item">
                                    <img class="img-icon" src={{$item['icon']}} />    
                                </div>
                            </div>                 
                            <div class="item-content">
                                <p class="subTitle">{{$item['subtitle']}}</p>
                                <p class="subContent">{{$item['subcontent']}}</p>
                                <p class="subContents">{{$item['subcontents']}}</p>
                            </div>
                            @if($item['subtitle'] !== 'Curator Title')
                            <img class="img-remove" src="assets/images/icon/remove.png" />
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection