<?php 
 $items = [
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist2.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist1.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist4.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist5.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist6.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist7.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist8.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist9.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist10.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist11.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist12.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist5.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'subcontent'=>'Collection Item',
        'subcontents'=>'Curator'
    ],
];
?>

<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/search.css?v='.time()) }}">
</head>
@extends('public.layout')
@section('title', 'searchCollection')
@section('content')
    <div class="searchCollection">
        <div class="container search-detail">
            <p class="title">All Collection for result <span class="font-weight-bold">Lee</span></p>
            <div class="list-all">
                @foreach($items as $item)
                    <div class="item-list">
                        <div class="image">
                            <a href="#"><img class="img" src={{$item['image']}} /></a>
                            <div class="icon-lists">
                                <img class="icon-play" src="assets/images/icon/play.png" />
                                <div class="icons">
                                    <img class="icon-like" src="assets/images/icon/like_grey.png" />
                                    <img class="icon-share" src="assets/images/icon/share_grey.png" />
                                    <img class="icon-addlist" src="assets/images/icon/addlist_grey.png" />
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <p class="contents">{{$item['subcontent']}}</p>
                            <p class="subcontent">{{$item['subcontents']}}</p>
                        </div>
                        <div class="extends">
                            <img src="assets/images/icon/extends.svg" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection