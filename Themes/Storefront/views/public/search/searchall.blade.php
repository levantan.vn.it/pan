<?php 
 $items = [
    [
        'titlePage'=>'Songs',
        'image'=>'assets/images/featureimg/playlist2.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Song Title',
        'subcontent'=>'SONG',
        'subcontents'=>''
    ],
    [
        'titlePage'=>'Collections',
        'image'=>'assets/images/featureimg/playlist3.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Collections Title',
        'subcontent'=>'CURATOR',
        'subcontents'=>'COLLECTIONS'
    ],
    [
        'titlePage'=>'Categories',
        'image'=>'assets/images/featureimg/playlist4.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Category Title',
        'subcontent'=>'SONG',
        'subcontents'=>''
    ],
    [
        'titlePage'=>'Playlist',
        'image'=>'assets/images/featureimg/playlist5.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Playlist Title',
        'subcontent'=>'AUTHOR PROFILE',
        'subcontents'=>'PLAYLIST'
    ],
    [
        'titlePage'=>'Curators',
        'image'=>'assets/images/featureimg/playlist6.png',
        'icon'=>'assets/images/icon/like_white.png',
        'subtitle'=>'Curator Name',
        'subcontent'=>'CURATOR',
        'subcontents'=>''
    ],
    [
        'titlePage'=>'Profile',
        'image'=>'assets/images/featureimg/playlist7.png',
        'icon'=>'assets/images/icon/play.png',
        'subtitle'=>'Other Profile Name',
        'subcontent'=>'PROFILE',
        'subcontents'=>''
    ],
    
];
?>

<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/search.css?v='.time()) }}">
</head>
@extends('public.layout')
@section('title', 'searchAll')
@section('content')
    <div class="searchAll">
        <div class="container">
            <div class="background-searches">
                <p class="title">Top Searches</p>
                <div class="top-searches">
                    <div class="image">
                        <img class="img" src="assets/images/featureimg/playlist1.png" />
                        <img class="icon_curator" src="assets/images/icon/like_white.png" />
                    </div>
                    <div class="names">
                        <p class="name-curator">Curator Name</p>
                        <p class="name">CURATOR</p>
                    </div>
                </div>
            </div>
            <div class="background-all">
                @foreach($items as $item)
                    <div class="background-song">
                        <div class="songs">
                            <div class="song">
                                <p class="title">{{$item['titlePage']}}</p>
                                <div class="see-all">
                                    <p class="text">See all <span>&raquo;</span></p>
                                </div>
                            </div>
                            <div class="list-song">
                                <div class="list">
                                    @for($i = 1; $i <= 3; $i++)
                                        <div class="list-item">
                                            <div class={{$item['subcontents'] === ''?'item':'items'}}> 
                                                <img class={{$item['subtitle'] ==='Curator Name'?'icon-curator':'img-icon'}} src={{$item['icon']}} />  
                                                <div class={{$item['subtitle'] === 'Curator Name'? 'image-curator':'image'}}>
                                                    <img class="img-item" src={{$item['image']}} />
                                                </div>
                                            </div>
                                            <div class="item-content">
                                                <p class={{$item['subcontents'] === ''?'subTitle':'subTitles'}}>{{$item['subtitle']}}</p>
                                                <p class="subContent">{{$item['subcontent']}}</p>
                                                <p class="subContents">{{$item['subcontents']}}</p>
                                            </div>     
                                        </div>
                                    @endfor 
                                </div>
                                <div class="list">
                                    @for($i = 1; $i <= 3; $i++)
                                        <div class="list-item">
                                            <div class={{$item['subcontents'] === ''?'item':'items'}}> 
                                                <img class={{$item['subtitle'] ==='Curator Name'?'icon-curator':'img-icon'}} src={{$item['icon']}} />  
                                                <div class={{$item['subtitle'] === 'Curator Name'? 'image-curator':'image'}}>
                                                    <img class="img-item" src={{$item['image']}} />
                                                </div>
                                            </div>
                                            <div class="item-content">
                                                <p class={{$item['subcontents'] === ''?'subTitle':'subTitles'}}>{{$item['subtitle']}}</p>
                                                <p class="subContent">{{$item['subcontent']}}</p>
                                                <p class="subContents">{{$item['subcontents']}}</p>
                                            </div>     
                                        </div>
                                    @endfor 
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                
            </div>
        </div>
    </div>
@endsection