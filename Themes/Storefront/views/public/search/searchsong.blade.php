<?php 
 $items = [
    [
        'No'=>'1.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'2.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'3.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'4.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'5.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'6.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'7.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'8.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'9.',
        'Title'=>'Song Item',
        'Duration'=>'2:25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'10.',
        'Title'=>'Song Item',
        'Duration'=>'2=>25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
    [
        'No'=>'11.',
        'Title'=>'Song Item',
        'Duration'=>'2=>25',
        'PlayedCounting'=>'145795',
        'player'=>'987.856 played'
    ],
];
?>

<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/search.css?v='.time()) }}">
</head>
@extends('public.layout')
@section('title', 'searchSong')
@section('content')
    <div class="searchSong">
        <div class="container search-detail">
            <p class="title">All Song for result <span>Lee</span></p>
            <div class="tables">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th class="icon"></th>
                            <th> Title </th>
                            <th> Duration </th>
                            <th> Played Counting Number </th>
                            <th>  </th>
                        </tr>
                    </thead> 
                    <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td class="font-weight-bold"> {{$item['No']}} </td>
                                <td class="td-icon">
                                    <button class="icon-play">
                                        <img src="assets/images/icon/play.png" />
                                    </button>
                                </td>
                                <td class="font-weight-bold bold">
                                    <p> {{$item['Title']}}</p>
                                    <p class="view-played">{{$item['player']}}</p> 
                                </td>
                                <td class="td-durator"> {{$item['Duration']}} </td>
                                <td class="td-played"> {{$item['PlayedCounting']}} </td>
                                <td>
                                    <div class="list">
                                        <button class="icon-like button">
                                            <img  src="assets/images/icon/like_blue.png" />
                                        </button>
                                        <button class="icon-share button">
                                            <img  src="assets/images/icon/share_blue.png" />
                                        </button>
                                        <button class="icon-addlist button">
                                            <img  src="assets/images/icon/addlist_blue.png" />
                                        </button>                          
                                    </div>
                                    <div class="list1">
                                        <button class="icons-play button">
                                            <img class="small"  src="assets/images/icon/play_small.png" />
                                            <img class="grey" src="assets/images/icon/play_grey.png" />
                                        </button>
                                        <button class="icon-like button">
                                            <img class="grey"  src="assets/images/icon/like_grey.png" />
                                            <img class="small" src="assets/images/icon/like_small.png" />
                                        </button>
                                        <button class="icon-share button">
                                            <img class="grey" src="assets/images/icon/share_grey.png" />
                                            <img class="small" src="assets/images/icon/share_small.png" />
                                        </button>
                                        <button class="icon-addlist button">
                                            <img class="grey" src="assets/images/icon/addlist_grey.png" />
                                            <img class="small" src="assets/images/icon/addlist_small.png" />
                                        </button>                          
                                    </div>
                                </td>
                            </tr> 
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection