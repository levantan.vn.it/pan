<?php 
 $items = [
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'subcontent'=>'Curator Name',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist2.png',
        'subcontent'=>'Curator Name',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist1.png',
        'subcontent'=>'Curator Name',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist4.png',
        'subcontent'=>'Curator Name',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist5.png',
        'subcontent'=>'Curator Name',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist6.png',
        'subcontent'=>'Curator Name',
        'subcontents'=>''
    ],
];
?>

<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/search.css?v='.time()) }}">
</head>
@extends('public.layout')
@section('title', 'searchCurator')
@section('content')
    <div class="searchCurator">
        <div class="container search-detail">
            <p class="title">All Curator for result <span class="font-weight-bold">Lee</span></p>
            <div class="list-all">
                @foreach($items as $item)
                    <div class="item-list">
                        <div class="image">
                            <a href="#"><img class="img" src={{$item['image']}} /></a>
                            <div class="icon-lists">
                                <img class="icon-like" src="assets/images/icon/like_white.png" />
                            </div>
                        </div>                   
                        <div class="list-content">
                            <p class="contents">{{$item['subcontent']}}</p>
                            {{-- <p class="subcontent">{{$item['subcontents']}}</p> --}}
                        </div>
                        <div class="extends">
                            <img src="assets/images/icon/extends.svg" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection