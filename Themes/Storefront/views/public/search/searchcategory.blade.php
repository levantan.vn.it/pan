<?php 
 $items = [
    [
        'image'=>'assets/images/featureimg/playlist3.png',
        'subcontent'=>'Onerepublic',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist2.png',
        'subcontent'=>'Chaff and dust',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist4.png',
        'subcontent'=>'Do it your way',
        'subcontents'=>''
    ],
    [
        'image'=>'assets/images/featureimg/playlist5.png',
        'subcontent'=>'Acidrap',
        'subcontents'=>''
    ],
];
?>

<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/search.css?v='.time()) }}">
</head>
@extends('public.layout')
@section('title', 'searchCategory')
@section('content')
    <div class="searchCategory">
        <div class="container search-detail">
            <p class="title">All Category for result <span class="font-weight-bold">Lee</span></p>
            <div class="list-all">
                @foreach($items as $item)
                    <div class="item-list">
                        <div class="image">
                            <a href="#"><img class="img" src={{$item['image']}} /></a>
                            <div class="icon-lists">
                                <img class="icon-play" src="assets/images/icon/play.png" />
                                <div class="icons">
                                    <img class="icon-like" src="assets/images/icon/like_grey.png" />
                                    <img class="icon-share" src="assets/images/icon/share_grey.png" />
                                    <img class="icon-addlist" src="assets/images/icon/addlist_grey.png" />
                                </div>
                            </div>
                        </div>
                        <div class="list-content">
                            <p class="contents">{{$item['subcontent']}}</p>
                            <p class="subcontent">{{$item['subcontents']}}</p>
                        </div>
                        <div class="extends">
                            <img src="assets/images/icon/extends.svg" />
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection