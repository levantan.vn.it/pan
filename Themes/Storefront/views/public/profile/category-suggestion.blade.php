@extends('public.profile.layout')

@section('content')
<div class="category_suggestion bg_grey">
		<div class="content">
            <a href="/" class="logo"><img src="assets/images/panmusic.png" alt="Pan Music"></a>
            <div class="header_page">
                <h2 class="title_page title_page_pc">Music Category Suggestion</h2>
                <h2 class="title_page title_page_mb">Category Suggestion</h2>
                <a href="{{route('curator-suggestion')}}"><button class="boxshadow btn_skip">Skip</button></a>
                <a href="{{route('curator-suggestion')}}"><button class="btn_purple boxshadow btn_next">NEXT</button></a>
            </div>
            <p class="desc_page">Choose your music taste and go with the flow</p>
            <div class="music_category">
                <div class="music_item">
                    <img src="assets/images/img_song1.png" alt="Song image" class="bg_img_music">
                    <p class="song">Bee Bee 1</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/img_song2.png" alt="Song image" class="bg_img_music">
                    <p class="song">CTV 3</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/img_song3.png" alt="Song image" class="bg_img_music">
                    <p class="song">Shania Twain</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/img_song4.png" alt="Song image" class="bg_img_music">
                    <p class="song">Starboy</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/img_song5.png" alt="Song image" class="bg_img_music">
                    <p class="song">Flame</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
            </div>

            <div class="header_business">
                <h2 class="title_page business title_page_pc">Business Category Suggestion</h2>
                <h2 class="title_page business title_page_mb">Business Category</h2>
                <p class="desc_page desc_business">Choose your music taste and go with the flow</p>
            </div>

            <div class="music_category">
                <div class="music_item">
                    <img src="assets/images/featureimg/playlist6.png" alt="Song image" class="bg_img_music">
                    <p class="song">Acidrap</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/featureimg/playlist10.png" alt="Song image" class="bg_img_music">
                    <p class="song">To africa with love</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/img_song3.png" alt="Song image" class="bg_img_music">
                    <p class="song">Onerepublic</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/featureimg/playlist14.png" alt="Song image" class="bg_img_music">
                    <p class="song">Chaff and dust</p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
                <div class="music_item">
                    <img src="assets/images/img_song1.png" alt="Song image" class="bg_img_music">
                    <p class="song">Bee Bee 1 </p>
                    <p class="singer">Major Lazer & DJ Snake</p>
                </div>
            </div>
            <div class="btn_bottom_mb">
                <a href="{{route('curator-suggestion')}}"><button class="boxshadow btn_skip skip_mb">Skip</button></a>
                <a href="{{route('curator-suggestion')}}"><button class="btn_purple boxshadow btn_next next_mb">Next</button></a></div>
            </div>
</div>
@endsection
