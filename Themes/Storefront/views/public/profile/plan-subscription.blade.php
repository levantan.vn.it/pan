@extends('public.profile.layout')

@section('content')
<div class="plan_subscription bg_grey">
		<div class="content">
            <a href="/" class="logo"><img src="assets/images/panmusic.png" alt="Pan Music"></a>
            <div class="header_page">
                <h2 class="title_page">Plan Subscription</h2>
                <a href="{{route('category-suggestion')}}"><button class="btn_purple boxshadow btn_next" >Next</button></a>
            </div>
            <p class="desc_page">Subscription your plan</p>
            <div class="plan_subs">
                <div class="plan_top">
                    <div class="plan_infor-wrap free_plan">
                        <label>
                            <img src="assets/images/bg_free_plan.png" alt="Free Plan">
                            <input type="radio" checked="checked" name="radio" class="radio-input">
                            <span class="checkmark"></span>
                            <div class="plan_infor">
                                <h3>FREE PLAN</h3>
                                <p class="description">Plan Description Here</p>
                            </div>
                        </label>
                    </div>
                    <div class="plan_infor-wrap paid_plan">
                        <label>
                            <img src="assets/images/bg_paid_plan.png" alt="Paid Plan">
                            <input type="radio" name="radio" class="radio-input">
                            <span class="checkmark"></span>
                            <div class="plan_infor paid">
                                <h3>PAID PLAN</h3>
                                <p class="description">Plan Description Here</p>
                                <p class="price">30.000 <span>KWON</span></p>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="plan_bottom">
                    <div class="plan_infor-wrap business_plan">
                        <label>
                            <img src="assets/images/bg_business_plan.png" alt="Business Plan">
                            <input type="radio" name="radio" class="radio-input">
                            <span class="checkmark"></span>
                            <div class="plan_infor business">
                                <h3>BUSINESS PLAN</h3>
                                <p class="description">Plan Description Here</p>
                                <p class="price">60.000 <span>KWON</span></p>
                            </div>
                        </label>
                    </div>
                </div>
                <a href="{{route('category-suggestion')}}"><button class="btn_purple boxshadow btn_next_mb" >Next</button></a>
            </div>
		</div>
</div>
@endsection
