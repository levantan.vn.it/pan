@extends('public.profile.layout')

@section('content')
<div class="curator_suggestion bg_grey">
		<div class="content">
            <a href="/" class="logo"><img src="assets/images/panmusic.png" alt="Pan Music"></a>
            <div class="header_page">
                <h2 class="title_page curator_title">Curator Suggestion</h2>
                <button class="boxshadow btn_skip">Skip</button>
                <button class="btn_purple boxshadow btn_next">START</button>
            </div>
            <p class="desc_page">These Curator made best music</p>
            <div class="music_category">
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator1.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Sia</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator2.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Martin Garrix</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator3.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Sasha Sloan</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator4.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Mariah Carey</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator5.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Selena gomez</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator6.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Mimy</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator7.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Goldlink</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator8.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Linking Park</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator9.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">Shades Of Love</p>
                    <p class="singer">134.454 Follows</p>
                </div>
                <div class="music_item">
                    <div class="img-wrap">
                        <img src="assets/images/curator/img_curator10.png" alt="Song image" class="bg_img_music">
                        <img src="assets/images/icon/like_white.png" alt="Song image" class="select_hover">
                    </div>
                    <p class="song">You</p>
                    <p class="singer">134.454 Follows</p>
                </div>
            </div>
            <div class="btn_bottom_mb">
                <a href="{{route('curator-suggestion')}}"><button class="boxshadow btn_skip skip_mb">Skip</button></a>
                <a href="{{route('curator-suggestion')}}"><button class="btn_purple boxshadow btn_next next_mb">Start</button></a>
            </div>
		</div>
</div>
@endsection
