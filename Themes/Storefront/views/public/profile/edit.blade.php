@extends('public.profile.layout')

@section('content')
<div class="profile page_user">
	<div class="form bg_grey">
		<div class="content">
			<a href="/" class="logo"><img src="assets/images/panmusic.png" alt="Pan Music"></a>
            <h2 class="title_page">Create Your Profile</h2>
            <p class="desc_page">Create your profile to get more taste of music you like</p>
			<form>
			  <div class="form-group avt" id="avt-wrap">
			    <img src="assets/images/icon/avt_profile.png" class="avatar" alt="No avatar" id="blah">
                <label for="file-input" class="file-avt">
                    <img src="assets/images/icon/Shape1.png" alt="" >
                    <input id="file-input" class="select-img" type="file"
                        accept=".png, .jpg, .jpeg, .gif" style="display:none;" onchange="readURL(this);">
                </label>
			  </div>
			  <div class="form-group">
			    <label for="fullname">Full Name</label>
			    <input type="text" name="fullname" class="form-control" id="fullname"  placeholder="Enter full name">
			  </div>
			  <div class="form-group pass">
			    <label for="birth-date">Date of Birth</label>
			    <div class="relative">
                <input type="text" name="birthdate" class="form-control" id="datepicker"  placeholder="Enter Date of Birth">
				</div>
			  </div>
			  <div class="form-group">
			    <label for="gender">Gender</label>
				<select class="form-control" id="gender" name="gender" aria-label="Default select example">
					<option>Male</option>
					<option>Female</option>
				</select>
				<span class="down-arrow"></span>
			  </div>
			  <div class="form-group">
			    <label for="country">Country</label>
				<select class="form-control" id="country" name="country" aria-label="Default select example">
					<option>Vietnam</option>
					<option>Korea</option>
				</select>
				<span class="down-arrow"></span>
			  </div>
			  <button type="submit" class="btn btn_purple boxshadow" id="btn_save">Save</button>
			  <button class="btn btn_purple boxshadow btn_create_business" id="btn_business_mb">Create Business Profile</button>
			  <div class="form-group skip" id="btn_skip">
			    <a href="#">Skip</a>
			  </div>
			</form>
		</div>
	</div>
	<div class="bg_img" id="bg_main">
		<div class="business_page" id="hide-main-page">
			<img class="bg_main_img" src="assets/images/bg_profile.png" alt="Profile">
			<div class="business">
				<h2 class="title_business">Business Profile</h2>
				<p class="desc_business">Create Business Profile to get music suitable for you business</p>
				<img src="assets/images/bg_main_profile.png" alt="Main Image" class="main-img"><br>
				<button type="submit" class="btn btn_purple boxshadow create_business" id="btn_business">Create Business Profile</button>
			</div>
		</div>
		<div class="business_wrap" id="business_wrap">
            <h2 class="title_page title_page_business">Create Business Profile</h2>
			<p class="desc_page desc_page_business">Create your profile to get more taste of music you like</p>
			<form class="business_form">
				<p class="title_upload_mb">Business registration form</p>
				<div class="upload-file">
					<label for="file-input-business">
					<img src="assets/images/icon/upload.png" class="file_business" alt="Upload">
						<input id="file-input-business" class="select-img" type="file" style="display:none;">
					</label>
					<p>Business registration form</p>
				</div>
				<div class="form-group-wrap">
					<div class="form-group business-infor">
						<label for="fullname">Business Name</label>
						<input type="text" name="businessname" class="form-control" id="businessname">
					</div>
					<div class="form-group business-infor">
						<label for="businesstype">Business Type</label>
						<select class="form-control" id="businesstype" name="businesstype" aria-label="Default select example">
							<option>Fashion</option>
						</select>
						<span class="down-arrow"></span>
					</div>
					<div class="form-group business-infor">
						<label for="representative">Representative</label>
						<input type="text" name="representative" class="form-control" id="representative">
					</div>
					<div class="form-group business-infor">
						<label for="email">Email</label>
						<input type="text" name="email" class="form-control" id="email">
					</div>
					<div class="form-group business-infor">
						<label for="businessaddress">Business Address</label>
						<input type="text" name="businessaddress" class="form-control" id="businessaddress">
					</div>
					<div class="form-group business-infor">
						<label for="businesshotline">Business Hotline</label>
						<input type="text" name="businesshotline" class="form-control" id="businesshotline">
					</div>
					<div class="form-group business-infor">
						<label for="storename">Business Name</label>
						<input type="text" name="storename" class="form-control" id="storename">
					</div>
					<div class="form-group business-infor">
						<label for="storeaddress">Store Area (optional)</label>
						<input type="text" name="storeaddress" class="form-control" id="storeaddress">
					</div>
					<button type="submit" class="btn btn_purple boxshadow save_business">Save</button>
					<span class="btn-cancel" id="cancel">Cancel</span>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   jQuery(document).ready( function() {
    $( "#datepicker" ).datepicker();
	$( "#btn_business" ).click(function() {
		$("#hide-main-page").css("display", "none");
		$("#business_wrap").css("display", "block");
	});
	$( "#btn_business_mb" ).click(function(event) {
		$("#hide-main-page").css("display", "none");
		$("#business_wrap").css("display", "block");
		$("#bg_main").css("display", "block");
		$("#btn_business_mb").hide();
		$("#btn_skip").hide();
		$("#btn_save").hide();
		event.preventDefault();

	});
	$( "#cancel" ).click(function() {
		$("#hide-main-page").css("display", "flex");
		$("#business_wrap").css("display", "none");
	});
  });

  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $("#blah").css({"border-radius" : "50%","width": "140px","height": "140px","object-fit":"cover"});
                $("#avt-wrap").css({"padding": "0", "border": "none"});
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
