<?php 
$item1=[
    [   
        'image'=>'/assets/images/featureimg/playlist1.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist2.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist3.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist4.png',
        'title'=>'Album',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist5.png',
        'title'=>'Girl band',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
];
$item2=[
    [   
        'image'=>'/assets/images/featureimg/playlist1.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist2.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist3.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist4.png',
        'title'=>'Album',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist5.png',
        'title'=>'Girl band',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist6.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist7.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist8.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ]
]
?>
@extends('public.layout')
@section('title', 'otherPage-curatorDetail')
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/library.css?v='.time())}}">
<link rel="stylesheet" href="{{asset('/assets/css/otherPage.css?v='.time())}}">
@endpush
@section('content')
<div class="library browse curator-detail">
    <div class="container">
        <div class=" menu-library menu_browse radius-30 bg_white pd-30">
            <div class="curator-img">
                <a href="#" class="image">
                    <img src={{asset('/assets/images/playlists/artist2.jpg')}} />
                </a>
            </div>
            <div class="curator-name">
                <p class="title">CURATOR</p>
                <p class="name">Curator Name</p>
                <p class="profile">Author Profile</p>
                <p class="following">143.345 Following</p>
                <div class="button-curator">
                    <button class="follow"><a href="#">Follow</a></button>
                    <button class="share-curator">
                        <a href="#"><img src={{asset('/assets/images/icon/share-other.png')}} /></a>
                    </button>
                </div>
            </div>
        </div>
        <div class="menu-collection menu_browse radius-30 bg_white">
            <h2 class="title">Latest Collection</h2>
            <div class="collection">
                <div class="curator-img">
                    <a href="#" class="image">
                        <img src={{asset('/assets/images/featureimg/playlist10.png')}} />
                    </a>
                </div>
                <div class="curator-name">
                    <p class="title">Collection Title</p>
                    <p class="date">Date Created: 12/12/2020</p>
                </div>
            </div>
        </div>
        <div class="browsemain">
            <div class="bg_white radius-30 browselist browselist_category browselist_category1 library-list1">
                <div class="heading">
                    <h2 class="title">Most Popular</h2>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($item1 as $item)
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src={{asset($item['image'])}} alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                            <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="contents">
                                    <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                    <p class="subtitle">{{$item['subtitle']}}</p>
                                    <p class="liked">{{$item['liked']}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="bg_white radius-30 browselist browselist_category browselist_category2 library-list2">
                <div class="heading">
                    <h2 class="title">Others</h2>
                </div>

                <div class="list_play">
                    <div class="list">
                        @foreach($item2 as $item)
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src={{asset($item['image'])}} alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="contents">
                                <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                <p class="subtitle">{{$item['subtitle']}}</p>
                                <p class="liked">{{$item['liked']}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        if($(window).innerWidth() < 992){
        $('.library-list1 .list').addClass('slider');
    }
    $('.slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
    {
      breakpoint: 380,
      settings: {
        slidesToShow: 2.5,
        slidesToScroll: 1,
      }
    }
        ]
    });
    });
</script>  
@endpush