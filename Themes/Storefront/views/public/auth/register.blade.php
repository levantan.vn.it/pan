@extends('public.auth.layout')

@section('content')
<div class="register page_user bg_grey">
	<div class="bg_img">
		<img src="assets/images/bg_intro_signup.jpg" alt="{{ trans('user::auth.sign_up') }}">
	</div>
	<div class="form">
		<div class="content">
			<a href="/" class="logo"><img src="assets/images/logo_pan.png" alt="Pan Music"></a>
			<h2 class="title_page">{{ trans('user::auth.sign_up') }}</h2>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" data-dismiss="alert" class="close">
                        <i class="las la-times"></i>
                    </button>

                    <i class="las la-check-circle"></i>

                    {{ session('success') }}
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" data-dismiss="alert" class="close">
                        <i class="las la-times"></i>
                    </button>

                    <i class="las la-check-circle"></i>

                    {{ session('error') }}
                </div>
            @endif
			<form id="register" method="POST" action="{{ route('register.post') }}">
                @csrf
			  <div class="form-group">
			    <label for="email">{{ trans('user::auth.email') }}</label>
			    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Example@gmail.com">
                @error('email')
                    <label class="error" for="email">{{ $message }}</label>
                @enderror
			  </div>
			  <div class="form-group">
			    <label for="confirmemail">{{ trans('user::auth.confirm_email') }}</label>
			    <input type="email" value="{{ old('confirmemail') }}" name="confirmemail" class="form-control" id="confirmemail" aria-describedby="emailHelp" placeholder="Example@gmail.com">
                @error('confirmemail')
                    <label class="error" for="confirmemail">{{ $message }}</label>
                @enderror
			  </div>
			  <div class="form-group pass">
			    <label for="password">{{ trans('user::auth.password') }}</label>
			    <div class="relative">
				    <input type="password" name="password" class="form-control" id="password" placeholder="{{ trans('user::auth.password') }}">
				    <span toggle="#password" class="eye field-icon toggle-password"></span>
                    @error('password')
                        <label class="error" for="password">{{ $message }}</label>
                    @enderror
				</div>
			  </div>
			  <div class="form-group confirmpass">
			    <label for="password_confirmation">{{ trans('user::auth.confirm_password') }}</label>
			    <div class="relative">
			    	<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="{{ trans('user::auth.confirm_password') }}">
			    	<span toggle="#password_confirmation" class="eye field-icon toggle-password"></span>
                    @error('password_confirmation')
                        <label class="error" for="password_confirmation">{{ $message }}</label>
                    @enderror
				</div>
			  </div>

			  <button type="submit" class="btn submit btn_signup btn_purple boxshadow">{{ trans('user::auth.sign_up') }}</button>
			</form>
			<div class="line"><span class="bg_grey">{{ trans('user::auth.or_sign_up_with') }}</span></div>
			<div class="login_social">
				<a href="#" class="login_fb boxshadow"><img src="assets/images/icon/icon_fb.png" alt="Facebook"/>Facebook</a>
				<a href="#" class="login_gg boxshadow"><img src="assets/images/icon/icon_gg.png" alt="Google"/>Google</a>
			</div>
			<div class="or">
				{{ trans('user::auth.already_have_account') }} <a href="{{route('login')}}">{{ trans('user::auth.sign_in_here') }}</a>
			</div>
		</div>
	</div>

</div>
@endsection
