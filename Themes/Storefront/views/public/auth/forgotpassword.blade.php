@extends('public.auth.layout')

@section('content')
<div class="page_user forgot_password bg_grey">
	<div class="content">
		<a href="/" class="logo"><img src="assets/images/logo_pan.png" alt="Pan Music"></a>
		<h2 class="title_page">Forgot Password</h2>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show">
                <button type="button" data-dismiss="alert" class="close">
                    <i class="las la-times"></i>
                </button>

                <i class="las la-check-circle"></i>

                {{ session('success') }}
            </div>
        @endif
		<form id="forgot_password" method="POST" action="{{route('reset.post')}}">
            @csrf
		  <div class="form-group">
		    <label for="email">Email</label>
		    <input type="email" name="email" class="form-control" value="{{ old('email') }}" id="email" aria-describedby="emailHelp" placeholder="Example@gmail.com">
		  </div>
		  <button type="submit" class="btn submit btn_forgot_pass btn_purple boxshadow">Send recovery email</button>
          @if(session()->has('error'))
                <p class="message_error center">{{ session('error') }}</p>
          @endif
		</form>
	</div>
</div>
@endsection
