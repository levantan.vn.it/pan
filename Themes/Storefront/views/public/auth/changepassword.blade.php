@extends('public.auth.layout')

@section('content')
<div class="page_user change_password bg_grey">
	<div class="content">
		<a href="/" class="logo"><img src="{{asset('assets/images/logo_pan.png')}}" alt="Pan Music"></a>
		<h2 class="title_page">{{ trans('user::auth.change_password') }}</h2>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show">
                <button type="button" data-dismiss="alert" class="close">
                    <i class="las la-times"></i>
                </button>

                <i class="las la-check-circle"></i>

                {{ session('success') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" data-dismiss="alert" class="close">
                    <i class="las la-times"></i>
                </button>

                <i class="las la-check-circle"></i>

                {{ session('error') }}
            </div>
        @endif
		<form id="change_password" method="POST" action="{{ route('reset.complete.post', [$user->email, $code]) }}">
            @csrf
		  <div class="form-group pass">
		    <label for="new_password">{{ trans('user::attributes.users.new_password') }}</label>
		    <div class="relative">
			    <input type="password" name="new_password" class="form-control" id="new_password">
			    <span toggle="#newpassword" class="eye field-icon toggle-password"></span>
                @error('new_password')
                    <label class="error">{{ $message }}</label>
                @enderror
			</div>
		  </div>
		  <div class="form-group confirmpass">
		    <label for="confirmpassword">{{ trans('user::attributes.users.confirm_new_password') }}</label>
		    <div class="relative">
		    	<input type="password" name="new_password_confirmation" class="form-control" id="confirmpassword">
		    	<span toggle="#confirmpassword" class="eye field-icon toggle-password"></span>
                @error('new_password_confirmation')
                    <label class="error">{{ $message }}</label>
                @enderror
			</div>
		  </div>
		  <button type="submit" class="btn submit btn_change_pass btn_purple boxshadow">{{ trans('user::auth.change_password') }}</button>

		</form>
	</div>
</div>
@endsection
