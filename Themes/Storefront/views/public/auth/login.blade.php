@extends('public.auth.layout')

@section('content')
<div class="login page_user bg_grey">
	<div class="bg_img">
		<img src="assets/images/bg_intro_signin.jpg" alt="Sign In">
	</div>
	<div class="form">
		<div class="content">
			<a href="/" class="logo"><img src="assets/images/logo_pan.png" alt="Pan Music"></a>
			<h2 class="title_page">{{ trans('user::auth.login') }}</h2>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" data-dismiss="alert" class="close">
                        <i class="las la-times"></i>
                    </button>

                    <i class="las la-check-circle"></i>

                    {{ session('success') }}
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" data-dismiss="alert" class="close">
                        <i class="las la-times"></i>
                    </button>

                    <i class="las la-check-circle"></i>

                    {{ session('error') }}
                </div>
            @endif
			<form id="login" method="POST" action="{{ route('login.post') }}">
                @csrf
                <input type="hidden" name="frontend" value="1">
			    <div class="form-group">
    			    <label for="email">{{ trans('user::auth.email') }}</label>
    			    <input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Example@gmail.com">
                    @error('email')
                        <label class="error" for="email">{{ $message }}</label>
                    @enderror
			    </div>
			    <div class="form-group pass">
    			    <label for="password">{{ trans('user::auth.password') }}</label>
    			    <div class="relative">
    				    <input type="password" name="password" class="form-control" id="password">
    				    <span toggle="#password" class="eye field-icon toggle-password"></span>
                        @error('password')
                            <label class="error" for="email">{{ $message }}</label>
                        @enderror
    				</div>
			    </div>
			    <div class="form-group">
  	                <a href="{{route('forgotpassword')}}" class="link_fgpassword">{{ trans('user::auth.forgot_password') }}</a>
			    </div>
			    <button type="submit" class="btn submit btn_signin btn_purple boxshadow">{{ trans('user::auth.sign_in') }}</button>
			</form>
			<div class="line"><span class="bg_grey">{{ trans('user::auth.or_sign_in_with') }}</span></div>
			<div class="login_social">
				<a href="#" class="login_fb boxshadow"><img src="assets/images/icon/icon_fb.png" alt="Facebook"/>Facebook</a>
				<a href="#" class="login_gg boxshadow"><img src="assets/images/icon/icon_gg.png" alt="Google"/>Google</a>
			</div>
			<div class="or">
				{{ trans('user::auth.dont_have_an_account') }} <a href="{{route('register')}}">{{ trans('user::auth.sign_up_here') }}</a>
			</div>
		</div>
	</div>
</div>
@endsection
