@extends('public.auth.layout')

@section('content')
<div class="page_user verify_email bg_grey">
	<div class="content">
		<a href="/" class="logo"><img src="assets/images/logo_pan.png" alt="Pan Music"></a>
		<h2 class="title_page">{{ trans('user::auth.verify_email') }}</h2>
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show">
                <button type="button" data-dismiss="alert" class="close">
                    <i class="las la-times"></i>
                </button>

                <i class="las la-check-circle"></i>

                {{ session('success') }}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" data-dismiss="alert" class="close">
                    <i class="las la-times"></i>
                </button>

                <i class="las la-check-circle"></i>

                {{ session('error') }}
            </div>
        @endif
		<p class="center intro">{{ trans('user::auth.an_verify_link_sent_email') }} <a href="mailto:{{$user->email}}">{{$user->email}}</a> {{ trans('user::auth.please_check_email_click_verify_link_confirm') }}</p>
        <form id="login" method="POST" action="{{ route('verifyemail.post') }}">
            @csrf
            <input type="hidden" name="user_id" value="{{ $user->id }}">
		    <button type="submit" class="btn submit btn_verify_email btn_purple boxshadow disabled">{{ trans('user::auth.send_again') }}</button>
        </form>
		<p class="center resend">{{ trans('user::auth.does_not_receive_yet') }} <strong>{{ trans('user::auth.resend_in') }} <span class="countdown">10</span>{{ trans('user::auth.seconds') }}</strong></p>
		</form>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    var counter = 10;
    var interval = setInterval(function() {
        counter--;
        $('.countdown').text(counter);
        // Display 'counter' wherever you want to display it.
        if (counter == 0) {
            $('.resend').remove();
            $('.btn_verify_email').removeClass('disabled');
            // Display a login box
            clearInterval(interval);
        }
    }, 1000);
</script>

@endsection
