<?php 
 $items1 = [
    [
        'title'=>'Best Mixes',
        'like'=>'123.321',
    ]      
     ,
    [  
        'title'=>'Madeaux',
        'like'=>'223.221',
    ],
    [  
        'title'=>'March',
        'like'=>'23.321',
    ],
    [   
        'title'=>'January',
        'like'=>'923.111',
    ],
    [   
        'title'=>'Jayball',
        'like'=>'12.321',
    ],
    [   
        'title'=>'Jayball',
        'like'=>'93.321',
    ],
    [   
        'title'=>'Dua',
        'like'=>'44.321',
    ],
    [   
        'title'=>'Best Mixes',
        'like'=>'83.321',
    ],
    [   
        'title'=>'Moutain',
        'like'=>'723.321',
    ], 
];
$items2=[
    [   
        'title'=>'YCEE',
        'like'=>'12.321',
    ],
    [   
        'title'=>'Blackpink',
        'like'=>'93.321',
    ],
    [   
        'title'=>'Beyonce',
        'like'=>'44.321',
    ],
    [   
        'title'=>'Wish ',
        'like'=>'83.321',
    ],
    [   
        'title'=>'Moon',
        'like'=>'723.321',
    ], 
]
?>
@extends('public.layout')
@section('title')
    My Page - My Playlist
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/mypage.css?v='.time())}}">
@endpush
@section('content')
<div class="my-page playlist">
    <div class="container">
        <div class="mypage-tabs box-shadow border-radius-30">
            <h2 class="title font-bold">My Page</h2>
            <div class="tabs-item tabs-pc">
                <a href="my-page/my-playlist" class="tabs-link font-bold {{Request::is('my-page/my-playlist') ? 'active' : ''}}">My Playlist</a>
                <a href="my-page/following" class="tabs-link font-bold {{Request::is('my-page/following') ? 'active' : ''}}">Following ( 45 )</a>
            </div>
            <div class="tabs-profile">
                <img src="{{asset('/assets/images/user.png')}}" alt="" class="image-profile">
                <div class="profile-name">
                    <p class="name font-bold">Quoc Nguyen</p>
                    <p class="my-profile">My Profile</p>
                </div>
                <img src="{{asset('/assets/images/icon/icon-more.svg')}}" alt="" class="icon-more">
            </div>
        </div>
        <div class="playlist-tabs">
            <div class="bg_white radius-30 public-playlist browselist box-shadow">
                <div class="tabs-item tabs-mobile">
                    <a href="my-page/my-playlist" class="tabs-link font-bold {{Request::is('my-page/my-playlist') ? 'active' : ''}}">My Playlist</a>
                    <a href="my-page/following" class="tabs-link font-bold {{Request::is('my-page/following') ? 'active' : ''}}">Following ( 45 )</a>
                </div>
                <div class="heading">
                    <h2 class="title">Public Playlist</h2>
                    <div class="arrow">
                        <div class="prev_slider1 prev_slider arrow_slider">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="next_slider1 next_slider arrow_slider active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="list_play">
                    <div class="list list_pc">
                        <div class="playitem">
                            <div class="playlist-generate">
                                <a href="" class="new">
                                    <div class="icon-add">
                                        <img src="{{asset('/assets/images/icon/add.png')}}" alt="">
                                    </div>
                                    <p class="new-title font-bold title-desktop">Create new playlist</p>
                                </a>
                                <p class="new-title font-bold title-mobile">Create new playlist</p>
                            </div>
                        </div>
                        @foreach($items1 as $item)
                        @php
                            $rand = random_int( 1, 10)
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="{{asset("/assets/images/img_song$rand.png")}}" alt="Playlist"></a>
                                <div class="more more_pc">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="{{asset('assets/images/icon/play.png')}}" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="{{asset('assets/images/icon/pen.svg')}}" alt="Edit" /></button>
                                        <button class="share"><img src="{{asset('assets/images/icon/lock.svg')}}" alt="Lock" /></button>
                                        <button class="addlist"><img src="{{asset('assets/images/icon/bin.svg')}}" alt="Delete" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                <p class="like">{{$item['like']}} Liked</p>
                            </div>
                            <div class="more more_mobile">
                                {{-- <button class="play" data-url="{{route('ajax.load-song')}}"><img src="{{asset('assets/images/icon/play.png')}}" alt="Play" /></button> --}}
                                <div class="list_function">
                                    <button class="like"><img src="{{asset('assets/images/icon/pen.svg')}}" alt="Edit" />Edit </button>
                                    <button class="share"><img src="{{asset('assets/images/icon/lock.svg')}}" alt="Lock" />Lock </button>
                                    <button class="addlist"><img src="{{asset('assets/images/icon/bin.svg')}}" alt="Delete" />Delete </button>
                                </div>
                            </div>
                            <a href="javascript:void(0)" class="btn-extends"><img src="{{asset('/assets/images/icon/extends.svg')}}" alt=""></a>
                        </div>
                        @endforeach
                    </div>
                    {{-- <div class="list list_mb">
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Onerepublic</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Chaff and dust</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Do it your way</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist4.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Flame</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">To africa with love</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Mimy</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
    
                    </div> --}}
                </div>
            </div>
            <div class="bg_white radius-30 private-playlist browselist box-shadow">
                <div class="heading">
                    <h2 class="title">Private Playlist</h2>
                </div>
                <div class="list_play">
                    <div class="list list_pc">
                        @foreach($items2 as $item)
                        @php
                            $rand = random_int(1, 10)
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="{{asset("/assets/images/img_song$rand.png")}}" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="{{asset('assets/images/icon/play.png')}}" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="{{asset('assets/images/icon/pen.svg')}}" alt="Edit" /></button>
                                        <button class="share"><img src="{{asset('assets/images/icon/lock.svg')}}" alt="Lock" /></button>
                                        <button class="addlist"><img src="{{asset('assets/images/icon/bin.svg')}}" alt="Delete" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                <p class="like">{{$item['like']}} Liked</p>
                            </div>
                            <a href="javascript:void(0)" class="btn-extends"><img src="{{asset('/assets/images/icon/extends.svg')}}" alt=""></a>
                        </div>
                        @endforeach
                    </div>
                    {{-- <div class="list list_mb">
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist1.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Onerepublic</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist2.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Chaff and dust</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist3.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Do it your way</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist4.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Flame</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist5.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">To africa with love</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="assets/images/featureimg/playlist6.png" alt="Playlist"></a>
                                <div class="more">
                                    <button class="play"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        <button class="addlist"><img src="assets/images/icon/addlist.png" alt="Add List" /></button>
                                    </div>
                                </div>
                            </div>
                            <h4 class="title"><a href="#">Mimy</a></h4>
                            <p class="author"><a href="#">Curator</a></p>
                            <p class="like">157.958 Liked</p>
                        </div>
    
                    </div> --}}
                </div>
            </div>
        </div>
    
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('.btn-extends').each(function(){
            $(this).click(function(){
                $(this).parent().find('.more_mobile').toggleClass('active');
            })
        })
    })
</script>
    
@endpush
