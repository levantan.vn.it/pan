<?php 
 $items1 = [
    [
        'title'=>'Say',
        'like'=>'123.321',
    ]      
     ,
    [  
        'title'=>'Relaxing',
        'like'=>'223.221',
    ],
    [  
        'title'=>'Cello',
        'like'=>'23.321',
    ],
    [   
        'title'=>'Covers',
        'like'=>'923.111',
    ],
    [   
        'title'=>'Mixtape',
        'like'=>'12.321',
    ],
    [   
        'title'=>'Wave',
        'like'=>'93.321',
    ],
    [   
        'title'=>'Lightning',
        'like'=>'44.321',
    ],
    [   
        'title'=>'5SOS',
        'like'=>'83.321',
    ],
    [   
        'title'=>'Juice',
        'like'=>'723.321',
    ], 
    [   
        'title'=>'Home',
        'like'=>'323.321',
    ], 
];
$items2=[
    [   
        'title'=>'Unknown',
        'like'=>'12.321',
    ],
    [   
        'title'=>'Se7en',
        'like'=>'93.321',
    ],
    [   
        'title'=>'Late Night',
        'like'=>'44.321',
    ],
    [   
        'title'=>'Deep',
        'like'=>'83.321',
    ],
    [   
        'title'=>'Allen',
        'like'=>'723.321',
    ], 
]
?>
@extends('public.layout')
@section('title')
    My Page - Following
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/mypage.css?v='.time())}}">
@endpush
@section('content')
<div class="my-page following">
    <div class="container">
        <div class="mypage-tabs box-shadow border-radius-30">
            <h2 class="title font-bold">My Page</h2>
            <div class="tabs-item tabs-pc">
                <a href="my-page/my-playlist" class="tabs-link font-bold {{Request::is('my-page/my-playlist') ? 'active' : ''}}">My Playlist</a>
                <a href="my-page/following" class="tabs-link font-bold {{Request::is('my-page/following') ? 'active' : ''}}">Following ( 45 )</a>
            </div>
            <div class="tabs-profile">
                <img src="{{asset('/assets/images/user.png')}}" alt="" class="image-profile">
                <div class="profile-name">
                    <p class="name font-bold">Quoc Nguyen</p>
                    <p class="my-profile">My Profile</p>
                </div>
                <img src="{{asset('/assets/images/icon/icon-more.svg')}}" alt="" class="icon-more">
            </div>
        </div>
        <div class="playlist-tabs">
            <div class="bg_white radius-30 public-playlist browselist box-shadow">
                <div class="tabs-item tabs-mobile">
                    <a href="my-page/my-playlist" class="tabs-link font-bold {{Request::is('my-page/my-playlist') ? 'active' : ''}}">My Playlist</a>
                    <a href="my-page/following" class="tabs-link font-bold {{Request::is('my-page/following') ? 'active' : ''}}">Following ( 45 )</a>
                </div>
                <div class="heading">
                    <h2 class="title">Following Curators</h2>
                    <div class="arrow">
                        <div class="prev_slider1 prev_slider arrow_slider">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="next_slider1 next_slider arrow_slider active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="list_play">
                    <div class="list list_pc round center">
                        @foreach($items1 as $item)
                        @php
                            $rand = random_int( 1, 10 )
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="{{asset("assets/images/featureimg/playlist$rand.png")}}" alt="Playlist"></a>
                                <div class="more"> 
                                    <div class="list_function">
                                        <button class="like"><img src="{{asset('assets/images/icon/like_white.png')}}" alt="Like" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                <p class="follow">{{$item['like']}} Follows</p>
                            </div>
                            <a href="javascript:void(0)" class="btn-extends"><img src="{{asset('/assets/images/icon/extends.svg')}}" alt=""></a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="bg_white radius-30 private-playlist browselist box-shadow">
                <div class="heading">
                    <h2 class="title">Following Profile</h2>
                </div>
                <div class="list_play">
                    <div class="list list_pc round center">
                        @foreach($items2 as $item)
                        @php
                            $rand = random_int( 1, 10 )
                        @endphp
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src="{{asset("assets/images/featureimg/playlist$rand.png")}}" alt="Playlist"></a>
                                <div class="more"> 
                                    <div class="list_function">
                                        <button class="like"><img src="{{asset('assets/images/icon/like_white.png')}}" alt="Like" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="song-detail">
                                <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                <p class="follow">{{$item['like']}} Follows</p>
                            </div>
                            <a href="javascript:void(0)" class="btn-extends"><img src="{{asset('/assets/images/icon/extends.svg')}}" alt=""></a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('.btn-extends').each(function(){
            $(this).click(function(){
                $(this).parent().find('.more_mobile').toggleClass('active');
            })
        })
    })
</script>
    
@endpush
