
@extends('public.layout')
@section('title')
    My Profile - Payment History
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/myprofile.css?v='.time())}}">
@endpush
@section('content')
<div class="my-profile payment-history">
    <div class="container">
        <div class="menu_browse radius-30 tabs-profile">
            <div class="profile-title">
                <h2 class="title">My Profile</h2>
            </div>
            <ul>
                <li class="{{Request::is('my-profile/profile-overview') ? 'active' : ''}}"><a href="{{ url('/my-profile/profile-overview') }}">Profile Overview</a></li>
                <li class="{{Request::is('my-profile/edit-profile') ? 'active' : ''}}"><a href="{{ url('/my-profile/edit-profile') }}">Edit Profile</a></li>
                <li class="{{Request::is('my-profile/change-password') ? 'active' : ''}}"><a href="{{ url('/my-profile/change-password') }}">Change Password</a></li>
                <li class="{{Request::is('my-profile/subscription-plan') ? 'active' : ''}}"><a href="{{ url('/my-profile/subscription-plan') }}">Subscription Plan</a></li>
                <li class="{{Request::is('my-profile/payment-history') ? 'active' : ''}}"><a href="{{ url('/my-profile/payment-history') }}">Payment History</a></li>
            </ul>
        </div>
        <div class="main-history">
                <div class="heading">
                    <h2 class="title">Payment History</h2>
                </div>
                <div class="list-payment list-payment-desk">
                    <table class="table table-payment">
                        <thead>
                          <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Transaction ID</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Billed</th>
                          </tr>
                        </thead>
                        <tbody>
                          @for($i=1;$i<=10;$i++)
                            <tr>
                                <td>12/12/2020</td>
                                <td>#Transactionid45645684958</td>
                                <td>30.000 KWON</td>
                                <td><a href="" class="detail-payment">Detail</a></td>
                            </tr>
                          @endfor
                        </tbody>
                      </table>
                </div>
                <div class="list-payment list-payment-mobile">
                    @for($i=1;$i<=10;$i++)
                    <div class="payment-history-item">
                        <div class="transaction">
                            <p class="name">#Transactionid</p>
                            <p class="price"><span class="price-number">30.000</span> KWON</p>
                        </div>
                        <div class="more">
                            <p class="date">12/12/2020</p>
                            <a href="" class="detial-payment">Detail</a>
                        </div>
                    </div>
                    @endfor
                </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
    })

</script>
    
@endpush