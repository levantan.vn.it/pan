
@extends('public.layout')
@section('title')
    My Profile - Profile Overview
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/myprofile.css?v='.time())}}">
@endpush
@section('content')
<div class="my-profile profile-overview">
    <div class="container">
        <div class="menu_browse radius-30 tabs-profile">
            <div class="profile-title">
                <h2 class="title">My Profile</h2>
            </div>
            <ul>
                <li class="{{Request::is('my-profile/profile-overview') ? 'active' : ''}}"><a href="{{ url('/my-profile/profile-overview') }}">Profile Overview</a></li>
                <li class="{{Request::is('my-profile/edit-profile') ? 'active' : ''}}"><a href="{{ url('/my-profile/edit-profile') }}">Edit Profile</a></li>
                <li class="{{Request::is('my-profile/change-password') ? 'active' : ''}}"><a href="{{ url('/my-profile/change-password') }}">Change Password</a></li>
                <li class="{{Request::is('my-profile/subscription-plan') ? 'active' : ''}}"><a href="{{ url('/my-profile/subscription-plan') }}">Subscription Plan</a></li>
                <li class="{{Request::is('my-profile/payment-history') ? 'active' : ''}}"><a href="{{ url('/my-profile/payment-history') }}">Payment History</a></li>
            </ul>
        </div>
        <div class="profile">
            <div class="user-profile">
                <div class="heading">
                    <h2 class="title">Profile Overview</h2>
                </div>
                <div class="user-infor">
                    <div class="avatar-left">
                        <img src="{{asset('/assets/images/avt.jpg')}}" alt="" class="avatar">
                    </div>
                    <div class="profile-right">
                        <h3 class="name">Quoc Nguyen</h3>
                        <p class="email">nguyenquoc1924@gmail.com</p>
                        <div class="coppy-profile">
                            {{-- <input type="text" id="coppy" value="http://pan.vietprojectgroup.com/my-profile/quoc-nguyen" id="myInput"> --}}
                            <a href="http://pan.vietprojectgroup.com/my-profile/quoc-nguyen" data-clipboard-target="#coppy" class="coppy-clipboard underline color-purple">Share profile link</a>
                            <span class="text">Click to copy to clipboard</span>
                        </div>
                    </div>
                </div>
                <div class="update-profile">
                    <form action="">
                        <div class="update-main">
                            <div class="update-profile-left">
                                <div class="form-update">
                                    <div class="profile-item">
                                        <label for="" class="label-pan">First Name</label>
                                        <input class="input-pan" type="text" name="" id="" value="Quoc">
                                    </div>
                                    <div class="profile-item" >
                                        <label for="" class="label-pan">Last Name</label>
                                        <input type="text"  class="input-pan" name="" id="" value="Nguyen">
                                    </div>
                                    <div class="profile-item">
                                        <label for="" class="label-pan">Date of Birth</label>
                                        <input type="text" class="input-pan" name="" id="" value="29/11/1991">
                                    </div>
                                    <div class="profile-item">
                                        <label for="" class="label-pan">Gender</label>
                                        <div class="dropdown dropdown-custom">
                                            <button class="select-custom dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Select Gender
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <ul>
                                                    <li class="dropdown-item selected" data-value="male">Male</li>
                                                    <li class="dropdown-item" data-value="female">Female</li>
                                                </ul>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="profile-item">
                                            <label for="" class="label-pan">Country</label>
                                            <div class="dropdown dropdown-custom">
                                                <button class="select-custom dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Select Country
                                                </button>
                                                <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                                    <ul>
                                                        <li class="dropdown-item selected" data-value="vietnam">Viet Nam</li>
                                                        <li class="dropdown-item" data-value="another">Another action</li>
                                                    </ul>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-plan-right">
                                <div class="plan-bg">
                                    <img src="{{asset('/assets/images/plan.png')}}" alt="">
                                </div>
                                <div class="plan-text">
                                    <h3 class="title">
                                        Free Plan
                                    </h3>
                                    <p>Plan Description here</p>
                                    <a href="" class="btn btn-change-plan btn-white">Change Plan</a>
                                </div>
                            </div>
                        </div>
                        <div class="btn-option">
                            <a href="javascript:void(0)" class="btn btn-edit btn-purple">Edit Profile</a>
                            <a href="javascript:void(0)" class="btn btn-sign-out btn-black">Sign Out</a>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="business-profile">
                <div class="heading">
                    <h2 class="title">Business Profile Overview</h2>
                </div>
                <form action="">
                    <div class="business-form">
                        <div class="business-item">
                            <label for="" class="label-pan">Business Name</label>
                            <input class="input-pan" type="text" name="" id="" value="Hanavasia">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Business Type</label>
                            <div class="dropdown dropdown-custom">
                                <button class="select-custom dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Select Type
                                </button>
                                <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                    <ul>
                                        <li class="dropdown-item selected" data-value="fashion">Fashion</li>
                                        <li class="dropdown-item" data-value="another">Another action</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Representative</label>
                            <input class="input-pan" type="text" name="" id="" value="Ms. Alice">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Email</label>
                            <input class="input-pan" type="text" name="" id="" value="Alice1991@gmail.com">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Address</label>
                            <input class="input-pan" type="text" name="" id="" value="31 Le Thach, W.12, D4, HCM ">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Hotline</label>
                            <input class="input-pan" type="text" name="" id="" value="+84 335 897 959 ">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Business Number</label>
                            <input class="input-pan" type="text" name="" id="" value="123456789">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Store Area (optional) </label>
                            <input class="input-pan" type="text" name="" id="" value="28 Dong Khoi, Ben Nghe W., D.1 ...">
                        </div>
                        <div class="business-item">
                            <label for="" class="label-pan">Business Registration Form</label>
                            <div class="business-images">
                                
                                @for($i=1;$i<=4;$i++)
                                <div class="overview-image">
                                    <a href="javascript:void(0)" class="image" data-toggle="modal" data-target="#exampleModal">
                                        <img src="{{asset("/assets/images/playlists/artist$i.jpg")}}" class="image-item" alt="">
                                    </a>
                                </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                    <div class="btn-option">
                        <a href="javascript:void(0)" class="btn btn-edit btn-purple">Edit Profile</a>
                        <a href="javascript:void(0)" class="btn btn-sign-out btn-black">Sign Out</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <img src="" class="preview" alt="">
               </div>
            </div>
          </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        var $temp = $("<input>");
        var $url = $('.coppy-clipboard').attr('href');
        $('.coppy-clipboard').click(function(e){
            e.preventDefault();
            $("body").append($temp);
            $temp.val($url).select();
            document.execCommand("copy");
            $temp.remove();
            $(".text").text("URL copied!");
        });
        $('.business-images .image').each(function(){
            $(this).click(function(){
                $('.preview').attr('src',$(this).find('img').attr('src'));
            })
        })

        $('.dropdown-custom .dropdown-item').click(function(){
            $(this).addClass('selected').siblings().removeClass('selected');
            $(this).parents('.dropdown-custom').find('.select-custom').html($(this).html());
        })
        let selected = $('.dropdown-custom .dropdown-item.selected').html();
        $('.dropdown-custom .dropdown-item.selected').each(function(){
            $(this).parents('.dropdown-custom').find('.select-custom').html($(this).html());

        })
    })

</script>
    
@endpush