
@extends('public.layout')
@section('title')
    My Profile - Subscription Plan
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/myprofile.css?v='.time())}}">
@endpush
@section('content')
<div class="my-profile subscription-plan">
    <div class="container">
        <div class="menu_browse radius-30 tabs-profile">
            <div class="profile-title">
                <h2 class="title">My Profile</h2>
            </div>
            <ul>
                <li class="{{Request::is('my-profile/profile-overview') ? 'active' : ''}}"><a href="{{ url('/my-profile/profile-overview') }}">Profile Overview</a></li>
                <li class="{{Request::is('my-profile/edit-profile') ? 'active' : ''}}"><a href="{{ url('/my-profile/edit-profile') }}">Edit Profile</a></li>
                <li class="{{Request::is('my-profile/change-password') ? 'active' : ''}}"><a href="{{ url('/my-profile/change-password') }}">Change Password</a></li>
                <li class="{{Request::is('my-profile/subscription-plan') ? 'active' : ''}}"><a href="{{ url('/my-profile/subscription-plan') }}">Subscription Plan</a></li>
                <li class="{{Request::is('my-profile/payment-history') ? 'active' : ''}}"><a href="{{ url('/my-profile/payment-history') }}">Payment History</a></li>
            </ul>
        </div>
        <div class="main-sub">
                <div class="heading">
                    <h2 class="title">Subscription Plan</h2>
                </div>
                <div class="list-plan">
                    <div class="plan-item current-plan">
                        <div class="plan-image">
                            <img src="{{asset('/assets/images/current-plan.png')}}" alt="">
                        </div>
                        <div class="plan-detail ">
                            <h3 class="title">Current plan</h3>
                            <p class="text">Plan Description here</p>
                            <p class="price-plan"><span class="kwon"><span class="price">30.000 </span>Kwon</span> /month</p>
                        </div>
                    </div>
                    <div class="plan-item free-plan">
                        <div class="plan-image">
                            <img src="{{asset('/assets/images/free-plan.png')}}" alt="">
                        </div>
                        <div class="plan-detail ">
                            <h3 class="title">Free Plan</h3>
                            <p class="text">Plan Description here</p>
                            <a href="" class="btn btn-change-plan btn-white">Change Plan</a>
                        </div>
                    </div>
                    <div class="plan-item other-plan">
                        <div class="plan-image">
                            <img src="{{asset('/assets/images/others-plan.png')}}" alt="">
                        </div>
                        <div class="plan-detail ">
                            <h3 class="title">other paid plan</h3>
                            <p class="text">Plan Description here</p>
                            <p class="price-plan"><span class="kwon"><span class="price">50.000 </span>Kwon</span> /month</p>
                            <a href="" class="btn btn-change-plan btn-white">Change Plan</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
    })

</script>
    
@endpush