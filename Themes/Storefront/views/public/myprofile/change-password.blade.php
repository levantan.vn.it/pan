
@extends('public.layout')
@section('title')
    My Profile - Change Password
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/myprofile.css?v='.time())}}">
@endpush
@section('content')
<div class="my-profile profile-password">
    <div class="container">
        <div class="menu_browse radius-30 tabs-profile">
            <div class="profile-title">
                <h2 class="title">My Profile</h2>
            </div>
            <ul>
                <li class="{{Request::is('my-profile/profile-overview') ? 'active' : ''}}"><a href="{{ url('/my-profile/profile-overview') }}">Profile Overview</a></li>
                <li class="{{Request::is('my-profile/edit-profile') ? 'active' : ''}}"><a href="{{ url('/my-profile/edit-profile') }}">Edit Profile</a></li>
                <li class="{{Request::is('my-profile/change-password') ? 'active' : ''}}"><a href="{{ url('/my-profile/change-password') }}">Change Password</a></li>
                <li class="{{Request::is('my-profile/subscription-plan') ? 'active' : ''}}"><a href="{{ url('/my-profile/subscription-plan') }}">Subscription Plan</a></li>
                <li class="{{Request::is('my-profile/payment-history') ? 'active' : ''}}"><a href="{{ url('/my-profile/payment-history') }}">Payment History</a></li>
            </ul>
        </div>
        <div class="form-password">
            <div class="pwd-section">
                <div class="heading">
                    <h2 class="title">Change Password</h2>
                </div>
                <div class="change-pwd">
                    <form action="">
                        <div class="update-main">
                                <div class="form-update">
                                    <div class="pwd-item">
                                        <label for="" class="label-pan">Old Password</label>
                                        <input class="input-pan pwd-field" type="password" name="" id="" value="Oldpasswordhere">
                                        <span class="icon-visible field-icon icon-pwd"></span>
                                    </div>
                                    <div class="pwd-item" >
                                        <label for="" class="label-pan">New Password</label>
                                        <input type="password"  class="input-pan pwd-field" name="" id="" value="Newpasswordhere">
                                        <span  class="icon-visible field-icon icon-pwd"></span>
                                    </div>
                                    <div class="pwd-item" >
                                        <label for="" class="label-pan">Confirm Password</label>
                                        <input type="password"  class="input-pan pwd-field" name="" id="" value="Newpasswordhere">
                                        <span class="icon-visible field-icon icon-pwd"></span>
                                    </div>
                                </div>
                        </div>
                        <div class="btn-option">
                            <a href="javascript:void(0)" class="btn btn-change-pwd btn-purple">Change Password</a>
                            <a href="javascript:void(0)" class="btn btn-cancel btn-black">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('.pwd-item .icon-pwd').each(function(){
            $(this).click(function(){
                $(this).toggleClass('icon-visible icon-visible-hidden');
                var input = $($(this).parent().find('.pwd-field'));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            })
        })
    })

</script>
    
@endpush