<?php 
$item1=[
    [   
        'image'=>'/assets/images/featureimg/playlist1.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist2.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist3.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist4.png',
        'title'=>'Album',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist5.png',
        'title'=>'Girl band',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist6.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist7.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist8.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist9.png',
        'title'=>'Album',
        'subtitle'=>'Author Profile'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist10.png',
        'title'=>'Girl band',
        'subtitle'=>'Author Profile'
    ],
];
$item2=[
    [   
        'image'=>'/assets/images/featureimg/playlist1.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist2.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist3.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist4.png',
        'title'=>'Album',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist5.png',
        'title'=>'Girl band',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist6.png',
        'title'=>'Best Mixes',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist7.png',
        'title'=>'Attention',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist8.png',
        'title'=>'Sweet love',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist9.png',
        'title'=>'Album',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist10.png',
        'title'=>'Girl band',
        'subtitle'=>'Author Profile',
        'liked'=>'157.958 Liked'
    ],
]
?>
@extends('public.layout')
@section('title', 'Library-likePlayList')
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/library.css?v='.time())}}">
@endpush
@section('content')
<div class="library browse">
    <div class="container">
        <div class=" menu-library menu_browse radius-30 bg_white pd-30">
            <div class="heading">
                <h2 class="title">Library</h2>
            </div>
            <ul>
                <li class="active"><a href="{{ url('/library/like-playlist') }}">Liked Playlist</a></li>
                <li><a href="{{ url('/library/like-collection') }}">Liked Collections</a></li>
                <li><a href="{{ url('/library/following-curator') }}">Following Curator ( 45 )</a></li>
            </ul>
        </div>

        <div class="browsemain">
            <div class="bg_white radius-30 browselist browselist_category browselist_category1 library-list1">
                <div class="heading">
                    <h2 class="title">Recently Played</h2>
                    <div class="arrow">
                        <div class="prev_slider1 prev_slider arrow_slider">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="next_slider1 next_slider arrow_slider active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($item1 as $item)
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src={{asset($item['image'])}} alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                        <div class="list_function">
                                            <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                            <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="contents">
                                    <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                    <p class="subtitle">{{$item['subtitle']}}</p>
                                </div>
                                <div class="extends">
                                    <img src="assets/images/icon/extends.svg" />
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="bg_white radius-30 browselist browselist_category browselist_category2 library-list2">
                <div class="heading">
                    <h2 class="title">Liked Playlists</h2>
                    <div class="arrow">
                        <div class="prev_slider2 prev_slider arrow_slider">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="next_slider2 next_slider arrow_slider active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>

                <div class="list_play">
                    <div class="list">
                        @foreach($item2 as $item)
                        <div class="playitem">
                            <div class="image">
                                <a href="#" class="img"><img src={{asset($item['image'])}} alt="Playlist"></a>
                                <div class="more">
                                    <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/play.png" alt="Play" /></button>
                                    <div class="list_function">
                                        <button class="like"><img src="assets/images/icon/like.png" alt="Like" /></button>
                                        <button class="share"><img src="assets/images/icon/share.png" alt="Share" /></button>
                                    </div>
                                </div>
                            </div>
                            <div class="contents">
                                <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                <p class="subtitle">{{$item['subtitle']}}</p>
                                <p class="liked">{{$item['liked']}}</p>
                            </div>
                            <div class="extends">
                                <img src="assets/images/icon/extends.svg" />
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection