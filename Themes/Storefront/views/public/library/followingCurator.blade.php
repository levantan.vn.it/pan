<?php 
$item1=[
    [   
        'image'=>'/assets/images/featureimg/playlist1.png',
        'title'=>'Say',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist2.png',
        'title'=>'Relaxing',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist3.png',
        'title'=>'Covers',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist4.png',
        'title'=>'Mixtape',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist5.png',
        'title'=>'Wave',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist6.png',
        'title'=>'Lightning',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist7.png',
        'title'=>'Blackpink',
        'subtitle'=>'Curator',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist8.png',
        'title'=>'5SOS',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist9.png',
        'title'=>'Juice ',
        'liked'=>'134.454 Follows'
    ],
    [   
        'image'=>'/assets/images/featureimg/playlist10.png',
        'title'=>'Home',
        'liked'=>'134.454 Follows'
    ],
];
?>
@extends('public.layout')
@section('title', 'Following-curator')
@push('styles')
<link rel="stylesheet" href="{{asset('/assets/css/library.css?v='.time())}}">
@endpush
@section('content')
<div class="library browse">
    <div class="container">
        <div class=" menu-library menu_browse radius-30 bg_white pd-30">
            <div class="heading">
                <h2 class="title">Library</h2>
            </div>
            <ul>
                <li><a href="{{ url('/library/like-playlist') }}">Liked Playlist</a></li>
                <li><a href="{{ url('/library/like-collection') }}">Liked Collections</a></li>
                <li class="active"><a href="{{ url('/library/following-curator') }}">Following Curator ( 45 )</a></li>
            </ul>
        </div>

        <div class="browsemain">
            <div class="bg_white radius-30 browselist browselist_category browselist_category1 library-list1 following">
                <div class="heading">
                    <h2 class="title">Following Curator ( 45 )</h2>
                    <div class="arrow">
                        <div class="prev_slider1 prev_slider arrow_slider">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="next_slider1 next_slider arrow_slider active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="list_play">
                    <div class="list">
                        @foreach($item1 as $item)
                            <div class="playitem">
                                <div class="image">
                                    <a href="#" class="img"><img src={{asset($item['image'])}} alt="Playlist"></a>
                                    <div class="more">
                                        <button class="play" data-url="{{route('ajax.load-song')}}"><img src="assets/images/icon/like_white.png" alt="Play" /></button>
                                    </div>
                                </div>
                                <div class="contents">
                                    <h4 class="title"><a href="#">{{$item['title']}}</a></h4>
                                    <p class="liked">{{$item['liked']}}</p>
                                </div>
                                <div class="extends">
                                    <img src="assets/images/icon/extends.svg" />
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection