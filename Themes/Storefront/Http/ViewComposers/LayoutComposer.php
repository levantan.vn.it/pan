<?php

namespace Themes\Storefront\Http\ViewComposers;

class LayoutComposer
{

    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param \Illuminate\View\View $view
     * @return void
     */
    public function compose($view)
    {
        $view->with([

        ]);
    }

}
