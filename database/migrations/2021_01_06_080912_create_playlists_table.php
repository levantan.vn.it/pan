<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlists', function (Blueprint $table) {
            $table->id();
            $table->enum('visibility',[1,2])->default(1)->comment("1=public, 2=private");
            $table->enum('status',[1,2])->default(1)->comment("1=active,2=deactive");
            $table->string('cover_image',100);
            $table->string('duration',100)->nullable();
            $table->integer('songs')->default(0);
            $table->integer('played')->default(0);
            $table->integer('likes')->default(0);
            $table->timestamps();
            $table->foreignId('account_id')->constrained('accounts')->onDelete('cascade');
            $table->foreignId('category_id')->constrained('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('playlists', function (Blueprint $table) {
            $table->dropForeign(['account_id']);
            $table->dropForeign(['category_id']);
        });

        Schema::dropIfExists('playlists');
    }
}
