<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->string('email',191)->unique();
            $table->enum('type',[1,2])->default(1)->comment("1=admin,2=curator");
            $table->string('password',100);
            $table->text('permission')->nullable();
            $table->integer('country_id')->default(0)->nullable();
            $table->enum('status',[0,1,2])->default(0)->comment("0=unverify,1=active,2=deactive");
            $table->datetime('last_login')->nullable();
            $table->string('avatar',100)->nullable();
            $table->integer('collections')->default(0);
            $table->integer('followers')->default(0);
            $table->integer('likes')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
