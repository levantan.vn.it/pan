<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('image',100)->nullable();
            $table->enum('type',[1,2])->default(1)->comment("1=normal, 2=business");
            $table->string('link');
            $table->enum('status',[1,2])->default(1)->comment("1=active,2=deactive");
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->integer('click')->default(0);
            $table->timestamps();
            $table->foreignId('account_id')->constrained('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropForeign(['account_id']);
        });
        Schema::dropIfExists('banners');
    }
}
