<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChartTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chart_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale',6);
            $table->string('name',255);
            $table->timestamps();
            $table->foreignId('chart_id')->constrained('charts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chart_translations', function (Blueprint $table) {
            $table->dropForeign(['chart_id']);
        });
        Schema::dropIfExists('chart_translations');
    }
}
