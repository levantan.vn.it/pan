<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plans', function (Blueprint $table) {
            $table->id();
            $table->enum('type',[1,2])->default(1)->comment("1=normal, 2=business");
            $table->double('price')->default(0);
            $table->integer('available_date')->default(0);
            $table->integer('current')->default(0);
            $table->integer('canceled')->default(0);
            $table->enum('status',[1,2])->default(1)->comment("1=active,2=deactive");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plans');
    }
}
