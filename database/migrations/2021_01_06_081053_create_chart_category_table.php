<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChartCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chart_category', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('chart_id')->constrained('charts')->onDelete('cascade');
            $table->foreignId('category_id')->constrained('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chart_category', function (Blueprint $table) {
            $table->dropForeign(['chart_id']);
            $table->dropForeign(['category_id']);
        });
        Schema::dropIfExists('chart_category');
    }
}
