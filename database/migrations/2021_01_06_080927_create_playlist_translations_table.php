<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaylistTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlist_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale',6);
            $table->string('name',255);
            $table->timestamps();
            $table->foreignId('playlist_id')->constrained('playlists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('playlist_translations', function (Blueprint $table) {
            $table->dropForeign(['playlist_id']);
        });
        Schema::dropIfExists('playlist_translations');
    }
}
