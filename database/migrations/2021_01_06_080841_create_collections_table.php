<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->enum('type',[1,2])->default(1)->comment("1=normal,2=business");
            $table->enum('status',[1,2])->default(1)->comment("1=active,2=deactive");
            $table->string('cover_image',100);
            $table->string('duration',100)->nullable();
            $table->integer('songs')->default(0);
            $table->integer('played')->default(0);
            $table->integer('likes')->default(0);
            $table->timestamp('released_at')->nullable();
            $table->timestamps();
            $table->foreignId('account_id')->constrained('accounts')->onDelete('cascade');
            $table->foreignId('category_id')->constrained('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropForeign(['account_id']);
            $table->dropForeign(['category_id']);
        });

        Schema::dropIfExists('collections');
    }
}
