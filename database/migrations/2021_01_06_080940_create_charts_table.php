<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charts', function (Blueprint $table) {
            $table->id();
            $table->enum('interval_update',[1,2,3,4])->default(1)->comment("1=date, 2=week, 3= month, 4=year");
            $table->enum('status',[1,2])->default(1)->comment("1=active,2=deactive");
            $table->string('cover_image',100);
            $table->integer('songs')->default(0);
            $table->integer('played')->default(0);
            $table->integer('likes')->default(0);
            $table->timestamps();
            $table->foreignId('account_id')->constrained('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('charts', function (Blueprint $table) {
            $table->dropForeign(['account_id']);
        });

        Schema::dropIfExists('charts');
    }
}
