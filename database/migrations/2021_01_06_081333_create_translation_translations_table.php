<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale',6);
            $table->string('value',255);
            $table->timestamps();
            $table->foreignId('translation_id')->constrained('translations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translation_translations', function (Blueprint $table) {
            $table->dropForeign(['translation_id']);
        });
        Schema::dropIfExists('translation_translations');
    }
}
