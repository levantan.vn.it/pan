<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->string('email',191)->unique();
            $table->enum('gender',[0,1,2])->default(0)->comment("0=undefind,1=male,2=female");
            $table->enum('user_type',[1,2])->default(1)->comment("1=normal, 2= business");
            $table->string('password',100);
            $table->date('birthday')->nullable();
            $table->integer('country_id')->default(0)->nullable();
            $table->integer('plan_id')->default(0)->nullable();
            $table->enum('status',[0,1,2])->default(0)->comment("0=unverify,1=active,2=deactive");
            $table->timestamp('email_verified_at')->nullable();
            $table->string('avatar',100)->nullable();
            $table->integer('playlists')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('followers')->default(0);
            $table->integer('following')->default(0);
            $table->string('facebook_id',255)->nullable();
            $table->string('google_id',255)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
