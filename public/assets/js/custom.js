$(document).ready(function(){
    // jQuery(".playitem .image .img img").lazyload({

    // });
    $('.banner_home').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots:true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrow: false
          }
        }
      ]
    });

    jQuery('.playlist_again .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider1'),
      prevArrow:$('.prev_slider1'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });

    jQuery('.category_shortcut_list .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider2'),
      prevArrow:$('.prev_slider2'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });
    jQuery('.recommendation_songs .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider3'),
      prevArrow:$('.prev_slider3'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });
    jQuery('.recommendation_collection .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider4'),
      prevArrow:$('.prev_slider4'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });
    jQuery('.recommendation_playlist .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider5'),
      prevArrow:$('.prev_slider5'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });

    jQuery('.recommendation_curator .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider6'),
      prevArrow:$('.prev_slider6'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });

    jQuery('.new_release .slider').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_slider7'),
      prevArrow:$('.prev_slider7'),
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrow: false,
            variableWidth: true,
            centerMode: false
          }
        }
      ]

    });

    jQuery('.chart_right .slider').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      dots: false,
      arrow: true,
      nextArrow:$('.next_sliderchart'),
      prevArrow:$('.prev_sliderchart')

    });

    // jQuery('button.play').on('click',function(){
    //     jQuery('.play-next-prev .jp-play').trigger( "click" );
    // });

    jQuery('.arrow_slider').on('click',function(){
        jQuery(this).parent().find('.arrow_slider').removeClass('active');
        if($(this).hasClass('active')){
          $(this).removeClass('active');
        }else{
          $(this).addClass('active');
        }
    });

    jQuery('.icon_menu').on('click',function(){
      jQuery('.header .menu').addClass('open');
    });
    jQuery('.close_menu').on('click',function(){
      jQuery('.header .menu').removeClass('open');
    });


    // add class active for menu
    jQuery(window).on('load',function(){
      var url = window.location.pathname;
      if(url.indexOf('browse') != -1){
        jQuery('.header .menu .menu_list li').find('a').removeClass('active');
        jQuery('.header .menu .menu_list li').eq(1).find('a').addClass('active');
      }

    });

    if(jQuery(window).width() < 767){
      jQuery('.menu_browse ul').slick({
        infinite: false,
        dots: false,
        arrow: false,
        slidesToShow: 1,
        centerMode: false,
        variableWidth: true

      });


        var urlfull = window.location.href;
        jQuery('.menu_browse ul li').each(function(){
          var link = jQuery(this).find('a').attr('href');
          var left = jQuery(this).position().left;
          console.log(left);
          if(urlfull==link){
            var positionslide = jQuery(this).attr('data-slick-index');
            console.log(positionslide);
            //jQuery('.menu_browse ul').scrollLeft(left - 30);
            // jQuery('.menu_browse ul li').removeClass('slick-current slick-active');
            // jQuery(this).addClass('slick-current slick-active');

            setTimeout(function() {
              jQuery('.menu_browse ul').slick("slickGoTo", positionslide, true);
            }, 500)


          }
        });



    }


});
