$(document).ready(function(){
	$( "#login" ).validate({
	  rules: {
	    email: {
	      required: true,
	      email: true
	    },
	    password: {
	      required: true
	    }
	  }
	});

	$( "#register" ).validate({
	  rules: {
	    email: {
	      required: true,
	      email: true
	    },
	    confirmemail: {
	      required: true,
	      email: true,
	      equalTo: "#email"
	    },
	    password: {
	      required: true
	    },
	    password_confirmation: {
	      required: true,
	      equalTo: "#password"
	    }
	  }
	});
	$( "#forgot_password" ).validate({
	  rules: {
	    email: {
	      required: true,
	      email: true
	    }
	  }
	});
	$( "#change_password" ).validate({
	  rules: {
	    new_password: {
	      required: true
	    },
	    new_password_confirmation: {
	      required: true,
	      equalTo: "#new_password"
	    }
	  }
	});

	$(".toggle-password").click(function() {
	  $(this).toggleClass("eye eye-slash");
	  var input = $($(this).attr("toggle"));
	  if (input.attr("type") == "password") {
	    input.attr("type", "text");
	  } else {
	    input.attr("type", "password");
	  }
	});
});
