<?php

namespace Modules\Page\Http\Controllers;
use Illuminate\Http\Request;

class AjaxController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function loadSongs(Request $request){
        $songs = \DB::table('songs')->select(['name as title','url as mp3','artist','poster','duration'])->orderBy('id', 'ASC')->paginate(100);
        return response()->json([
            'data' => $songs
        ]);
    }

    public function loadSongFirst(Request $request){
        $songs = \DB::table('songs')->select(['name as title','url as mp3','artist','poster'])->first();
        return response()->json([
            'data' => $songs
        ]);
    }

}
