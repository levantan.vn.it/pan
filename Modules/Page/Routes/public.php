<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', function () {
    return view('public.home.home');
})->name('home.index');
Route::get('/collection', function () {
    return view('public.collection.index');
});
Route::post('ajax/load-songs', 'AjaxController@loadSongs')->name('ajax.load-song');
Route::post('ajax/load-song-first', 'AjaxController@loadSongFirst')->name('ajax.load-song-first');

Route::get('/plan-subscription', function () {
    return view('public.profile.plan-subscription');
});
Route::get('create-profile', function () {
    return view('public.profile.edit');
});
Route::get('/category-suggestion', function () {
    return view('public.profile.category-suggestion');
})->name('category-suggestion');
Route::get('/curator-suggestion', function () {
    return view('public.profile.curator-suggestion');
})->name('curator-suggestion');

// Browse
Route::get('/browse', function () {
    return view('public.browse.category');
});
Route::get('/browse/category', function () {
    return view('public.browse.category');
});
Route::get('/browse/collection', function () {
    return view('public.browse.collection');
});
Route::get('/browse/playlist', function () {
    return view('public.browse.playlist');
});
Route::get('/browse/curator', function () {
    return view('public.browse.curator');
});
Route::get('/browse/chart', function () {
    return view('public.browse.chart');
});
Route::get('/browse/new-release', function () {
    return view('public.browse.new-release');
});

// Search
Route::get('/search', function () {
    return view('public.search.search');
});
Route::get('/search/all', function () {
    return view('public.search.searchall');
});
Route::get('/search/song', function () {
    return view('public.search.searchsong');
});
Route::get('/search/collection', function () {
    return view('public.search.searchcollection');
});
Route::get('/search/category', function () {
    return view('public.search.searchcategory');
});
Route::get('/search/curator', function () {
    return view('public.search.searchcurator');
});
Route::get('/search/profile', function () {
    return view('public.search.searchprofile');
});
Route::get('/my-page/my-playlist', function () {
    return view('public.mypage.myplaylist');
});
Route::get('/my-page/following', function () {
    return view('public.mypage.following');
});
Route::get('/library/like-playlist', function () {
    return view('public.library.likePlayList');
});
Route::get('/library/like-collection', function () {
    return view('public.library.likeCollection');
});
Route::get('/library/following-curator', function () {
    return view('public.library.followingCurator');
});
Route::get('/my-profile/profile-overview', function () {
    return view('public.myprofile.profile-overview');
});
Route::get('/my-profile/edit-profile', function () {
    return view('public.myprofile.edit-profile');
});
Route::get('/my-profile/change-password', function () {
    return view('public.myprofile.change-password');
});
Route::get('/otherPage/curator-detail', function () {
    return view('public.otherPage.curator-detail');
});
Route::get('/my-profile/subscription-plan', function () {
    return view('public.myprofile.subscription-plan');
});
Route::get('/my-profile/payment-history', function () {
    return view('public.myprofile.payment-history');
});
Route::get('/category', function () {
    return view('public.category.index');
});
