<?php

namespace Modules\Slider\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item(trans('slider::sliders.banner'), function (Item $item) {
                $item->weight(10);
                $item->icon('fa fa-folder');
                // $item->route('admin.sliders.index');
                $item->authorize(
                    $this->auth->hasAccess('admin.categories.index')
                );
            });
        });
    }
}
