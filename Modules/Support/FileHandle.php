<?php

namespace Modules\Support;

use Illuminate\Support\Facades\Storage;

class FileHandle
{
    /**
     * Upload privacy video
     *
     * @param $file - Example: $file = $request->file('image')
     * @param null $folder - Example: avatars or images
     *
     * @return string
     */
    public static function uploadImage($file, $folder = null): string
    {

        $path = '/images' . ($folder ? "/$folder" : "");

        if (config('filesystems.default') === 'local') {
            $path = "/public" . $path;
        }

        return Storage::disk(config('filesystems.default'))->put($path, $file);
    }

    /**
     * Delete file
     *
     * @param string|null $path
     *
     * @return bool
     */
    public static function delete(string $path = null): bool
    {
        return $path ? Storage::delete($path) : true;
    }


    /**
     * Get asset url
     *
     * @param $path
     * @return string
     */
    public static function getPath($path): string
    {
        return asset(Storage::disk(config('filesystems.default'))->url($path));
    }
}
