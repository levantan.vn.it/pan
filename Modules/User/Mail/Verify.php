<?php

namespace Modules\User\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Modules\Media\Entities\File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Verify extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $user;
    public $heading;
    public $text;
    public $link;

    /**
     * Create a new instance.
     *
     * @param string $firstName
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->heading = trans('user::mail.verify', ['name' => setting('store_name')]);
        $this->text = trans('user::mail.account_created');
        $this->link = trans('user::mail.link_verify');
        $this->link .= '<a href="'.route('register.verify',['user_id'=>$user->id,'code'=>$user->verify_code]).'">'.trans('user::mail.click_here').'</a>';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('user::mail.verify', ['name' => setting('store_name')]))
            ->view("emails.{$this->getViewName()}", [
                'logo' => File::findOrNew(setting('storefront_mail_logo'))->path,
            ]);
    }

    private function getViewName()
    {
        return 'verify';
    }
}
