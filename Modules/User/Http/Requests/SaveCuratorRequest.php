<?php

namespace Modules\User\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Http\Requests\Request;
use Modules\Support\Country;

class SaveCuratorRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'user::attributes.users';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'nullable|image',
            'name' => 'required|string|min:3|max:191',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'country_id' => 'nullable|' . Rule::in(Country::codes()),
            'status' => 'required|in:0,1,2'
        ];
    }
}
