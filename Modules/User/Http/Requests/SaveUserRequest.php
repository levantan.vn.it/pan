<?php

namespace Modules\User\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Http\Requests\Request;
use Modules\Support\Country;

class SaveUserRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'user::attributes.users';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users.avatar' => 'nullable|image',
            'users.name' => 'required|string|min:3|max:191',
            'users.email' => ['required', 'email', $this->emailUniqueRule()],
            'users.user_type' => 'required|in:1,2',
            'users.birthday' => 'nullable|date|before:-6 years|after:-100 years',
            'users.country_id' => 'nullable|' . Rule::in(Country::codes()),
            'users.plan_id' => 'nullable|exists:subscription_plans,id',
            'users.status' => 'required|in:0,1,2',
            'businesses' => 'sometimes',
            'businesses.business_name' => 'sometimes|required|string|max:191',
            'businesses.business_type' => 'sometimes|required|string|max:191',
            'businesses.representative' => 'sometimes|required|string|max:191',
            'businesses.business_email' => 'sometimes|required|email|max:191',
            'businesses.address' => 'sometimes|required|string|max:191',
            'businesses.hotline' => 'sometimes|required|regex:/[0-9]{6,20}/|max:20',
            'businesses.store_name' => 'sometimes|required|string|max:191',
            'businesses.store_address' => 'sometimes|required|string|max:191'
        ];
    }

    private function emailUniqueRule()
    {
        $rule = Rule::unique('users');

        if ($this->route()->getName() === 'admin.users.update') {
            $userId = $this->route()->parameter('id');

            return $rule->ignore($userId);
        }

        return $rule;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'users' => [
                'avatar' => $this->avatar,
                'name' => $this->name,
                'email' => $this->email,
                'user_type' => $this->user_type,
                'birthday' => $this->birthday,
                'country_id' => $this->country_id,
                'plan_id' => $this->plan_id,
                'status' => $this->status
            ]
        ]);
        if ($this->user_type == 2 && $this->has('business_name')) {
            $this->merge([
                'businesses' => [
                    'business_name' => $this->business_name,
                    'business_type' => $this->business_type,
                    'representative' => $this->representative,
                    'business_email' => $this->business_email,
                    'address' => $this->address,
                    'hotline' => $this->hotline,
                    'store_name' => $this->store_name,
                    'store_address' => $this->store_address,
                ]
            ]);
        }
    }
}
