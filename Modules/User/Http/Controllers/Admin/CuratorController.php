<?php

namespace Modules\User\Http\Controllers\Admin;

use Modules\User\Entities\User;
use Modules\Admin\Traits\HasCrudActions;
use Modules\User\Http\Requests\SaveCuratorRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Modules\Support\FileHandle;
use Modules\User\Admin\CuratorExport;
use Modules\User\Admin\CuratorTable;

class CuratorController
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'user::users.curators';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'user::admin.curators';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveUserRequest::class;

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() || $request->has('excel')) {
            $query = $this->getModel()->typeCurator()->when($request->search, function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('id', 'like', "%{$request->key}%")
                        ->orWhere('name', 'like', "%{$request->key}%")
                        ->orWhere('email', 'like', "%{$request->key}%");
                });

                if ($request->status && in_array(intval($request->status), [0, 1, 2], true)) {
                    $query->where('status', "{$request->status}");
                }

                if ($request->created_at[0] || $request->created_at[1]) {
                    $created_at = [
                        $request->created_at[0] ?? 0,
                        $request->created_at[1] ?? Date('Y-m-d'),
                    ];
                    $query->whereBetween('created_at', $created_at);
                }

                if (in_array(strtolower($request->followers), ['asc', 'desc'])) {
                    $query->orderBy('followers', $request->followers);
                }
            })->latest();



            return $request->has('excel') ? CuratorExport::excel($query) : new CuratorTable($query);
        }

        return view("{$this->viewPath}.index");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Modules\User\Http\Requests\SaveCuratorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveCuratorRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);

        $user = User::create($request->all());

        $user->roles()->attach($request->roles);

        Activation::complete($user, Activation::create($user)->code);

        return redirect()->route('admin.users.index')
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => trans('user::users.user')]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param \Modules\User\Http\Requests\SaveCuratorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(SaveCuratorRequest $request, $id)
    {
        $curator = User::typeCurator()->findOrFail($id);
        $data = $request->validated();
        if ($request->hasFile('avatar')) {
            $data['avatar'] = FileHandle::uploadImage($request->file('avatar'), 'avatars');
            FileHandle::delete($curator->avatar);
        } else {
            unset($data['avatar']);
        }

        $curator->update($data);

        return redirect()->route('admin.curators.index')
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => trans('user::curators.user')]));
    }

    /**
     * Get data to edit
     * @param int $id
     */
    public function edit($id)
    {
        $curator = User::typeCurator()->findOrFail($id);
        return view('user::admin.curators.detail', compact('curator'));
    }
}
