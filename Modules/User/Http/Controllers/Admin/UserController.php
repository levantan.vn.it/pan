<?php

namespace Modules\User\Http\Controllers\Admin;

use Modules\User\Entities\User;
use Modules\Admin\Traits\HasCrudActions;
use Modules\User\Http\Requests\SaveUserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Modules\Support\FileHandle;
use Modules\User\Admin\UserExport;
use Modules\User\Admin\UserTable;
use Modules\User\Entities\SubscriptionPlan;
use Modules\User\Entities\UserPlan;

class UserController
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'user::users.user';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'user::admin.users';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveUserRequest::class;

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() || $request->has('excel')) {
            $query = $this->getModel()->typeUser()->when($request->search, function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('id', 'like', "%{$request->key}%")
                        ->orWhere('name', 'like', "%{$request->key}%")
                        ->orWhere('email', 'like', "%{$request->key}%");
                });

                if ($request->status && in_array(intval($request->status), [0, 1, 2], true)) {
                    $query->where('status', "{$request->status}");
                }

                if ($request->user_type && in_array($request->user_type, [1, 2])) {
                    $query->where('user_type', $request->user_type);
                }

                if ($request->plan_id) {
                    $query->where('plan_id', $request->plan_id);
                }

                if ($request->created_at[0] || $request->created_at[1]) {
                    $created_at = [
                        $request->created_at[0] ?? 0,
                        $request->created_at[1] ?? Date('Y-m-d'),
                    ];
                    $query->whereBetween('created_at', $created_at);
                }

                if (in_array(strtolower($request->followers), ['asc', 'desc'])) {
                    $query->orderBy('followers', $request->followers);
                }
            })->latest();

            return $request->has('excel') ? UserExport::excel($query) : new UserTable($query);
        }

        $plans = SubscriptionPlan::with('plan_translation')->get();

        return view("{$this->viewPath}.index", compact('plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Modules\User\Http\Requests\SaveUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);

        $user = User::create($request->all());

        $user->roles()->attach($request->roles);

        Activation::complete($user, Activation::create($user)->code);

        return redirect()->route('admin.users.index')
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => trans('user::users.user')]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param \Modules\User\Http\Requests\SaveUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(SaveUserRequest $request, $id)
    {
        $user = User::with('business')->typeUser()->findOrFail($id);
        $data = $request->validated();
        if ($request->hasFile('avatar')) {
            $data['users']['avatar'] = FileHandle::uploadImage($request->file('avatar'), 'avatars');
            FileHandle::delete($user->avatar);
        } else {
            unset($data['users']['avatar']);
        }

        $user->update($data['users']);

        if (array_key_exists('businesses', $data)) {
            if ($user->business) {
                $user->business->update($data['businesses']);
            } else {
                $user->business()->create($data['businesses']);
            }
        } else if ($user->business && $request->user_type == 1) {
            $user->business->delete();
        }

        return redirect()->route('admin.users.index')
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => trans('user::users.user')]));
    }

    /**
     * Get data to edit
     * @param int $id
     */
    public function edit($id)
    {
        $user = User::with('business', 'transactions', 'plans', 'plans.plan_translation')->typeUser()->findOrFail($id);
        $business = $user->business;
        $transactions = $user->transactions;
        $plan = SubscriptionPlan::with('plan_translation')->where('id', $user->plan_id)->first();
        $plans = $user->plans;
        return view('user::admin.users.detail', compact('user', 'business', 'plan', 'transactions', 'plans'));
    }
}
