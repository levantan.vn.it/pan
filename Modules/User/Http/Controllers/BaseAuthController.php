<?php

namespace Modules\User\Http\Controllers;

use Modules\User\Mail\Welcome;
use Modules\User\Entities\Role;
use Modules\User\Entities\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\User\Mail\ResetPasswordEmail;
use Modules\User\Contracts\Authentication;
use Modules\User\Http\Requests\LoginRequest;
use Modules\User\Http\Requests\RegisterRequest;
use Modules\User\Http\Requests\PasswordResetRequest;
use Modules\User\Http\Requests\ResetCompleteRequest;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Modules\User\Mail\Verify;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
abstract class BaseAuthController extends Controller
{
    /**
     * The Authentication instance.
     *
     * @var \Modules\User\Contracts\Authentication
     */
    protected $auth;

    /**
     * @param \Modules\User\Contracts\Authentication $auth
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;

        $this->middleware('guest')->except('getLogout');
    }

    /**
     * Where to redirect users after login..
     *
     * @return string
     */
    abstract protected function redirectTo();

    /**
     * The login route.
     *
     * @return string
     */
    abstract protected function loginUrl();

    /**
     * Show login form.
     *
     * @return \Illuminate\Http\Response
     */
    abstract public function getLogin();

    /**
     * Show reset password form.
     *
     * @return \Illuminate\Http\Response
     */
    abstract public function getReset();

    /**
     * Login a user.
     *
     * @param \Modules\User\Http\Requests\LoginRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(LoginRequest $request)
    {
        try {
            $loggedIn = $this->auth->login([
                'email' => $request->email,
                'password' => $request->password,
            ], (bool) $request->get('remember_me', false));

            if (! $loggedIn) {
                return back()->withInput()
                    ->withError(trans('user::messages.users.invalid_credentials'));
            }

            return redirect()->intended($this->redirectTo());
        } catch (NotActivatedException $e) {
            return redirect()->route('verifyemail')
                ->with(['verifyEmail'=>$request->email]);
            // return back()->withInput()
            //     ->withError(trans('user::messages.users.account_not_activated'));
        } catch (ThrottlingException $e) {
            return back()->withInput()
                ->withError(trans('user::messages.users.account_is_blocked', ['delay' => $e->getDelay()]));
        }
    }

    /**
     * Logout current user.
     *
     * @return void
     */
    public function getLogout()
    {
        $this->auth->logout();

        return redirect($this->loginUrl());
    }

    /**
     * Register a user.
     *
     * @param \Modules\User\Http\Requests\RegisterRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(RegisterRequest $request)
    {
        $access_token = 'PanMusic'.str_random(10);
        $base64_encode  = base64_encode($access_token);
        $request->merge(['verify_code' => $base64_encode]);
        $request->merge(['status' => '0']);
        $request->merge(['user_type' => '1']);
        $request->merge(['type' => '1']);
        $user = $this->auth->register($request->only([
            'email',
            'password',
            'verify_code',
            'status',
            'user_type',
            'type'
        ]));

        Mail::to($request->email)
                ->send(new Verify($user));

        return redirect()->route('verifyemail')
            ->with(['verifyEmail'=>$request->email]);
    }

    public function sendVerifyEmail(Request $request)
    {
        $access_token = 'PanMusic'.str_random(10);
        $base64_encode  = base64_encode($access_token);
        $user = User::find($request->user_id);
        if(empty($user)){
            return back()->withError(trans('user::messages.users.no_user_found'));
        }
        $user->verify_code = $base64_encode;
        $user->save();

        Mail::to($user->email)
                ->send(new Verify($user));

        return redirect()->route('verifyemail')
            ->with(['verifyEmail'=>$user->email])->withSuccess(trans('user::messages.users.resend_email_success'));
    }

    protected function assignCustomerRole($user)
    {
        $role = Role::findOrNew(setting('customer_role'));

        if ($role->exists) {
            $this->auth->assignRole($user, $role);
        }
    }



    /**
     * Start the reset password process.
     *
     * @param \Modules\User\Http\Requests\PasswordResetRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(PasswordResetRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (is_null($user)) {
            return back()->withInput($request->input())
                ->withError(trans('user::messages.users.no_user_found'));
        }

        if (empty($user->status)) {
            return back()->withInput($request->input())
                ->withError(trans('user::messages.users.account_not_activated'));
        }
        $code = $this->auth->createReminderCode($user);

        Mail::to($user)
            ->send(new ResetPasswordEmail($user, $this->resetCompleteRoute($user, $code)));

        return back()->withSuccess(trans('user::messages.users.check_email_to_reset_password'));
    }

    /**
     * Reset complete form route.
     *
     * @param \Modules\User\Entities\User $user
     * @param string $code
     * @return string
     */
    abstract protected function resetCompleteRoute($user, $code);

    /**
     * Password reset complete view.
     *
     * @return string
     */
    abstract protected function resetCompleteView();

    /**
     * Show reset password complete form.
     *
     * @param string $email
     * @param string $code
     * @return \Illuminate\Http\Response
     */
    public function getResetComplete($email, $code)
    {
        $user = User::where('email', $email)->firstOrFail();

        if ($this->invalidResetCode($user, $code)) {
            return redirect()->route('reset')
                ->withError(trans('user::messages.users.invalid_reset_code'));
        }

        return $this->resetCompleteView()->with(compact('user', 'code'));
    }

    /**
     * Determine the given reset code is invalid.
     *
     * @param \Modules\User\Entities\User $user
     * @param string $code
     * @return bool
     */
    private function invalidResetCode($user, $code)
    {
        return $user->reminders()->where('code', $code)->doesntExist();
    }

    /**
     * Complete the reset password process.
     *
     * @param string $email
     * @param string $code
     * @param \Modules\User\Http\Requests\ResetCompleteRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postResetComplete($email, $code, ResetCompleteRequest $request)
    {
        $user = User::where('email', $email)->firstOrFail();

        $completed = $this->auth->completeResetPassword($user, $code, $request->new_password);
        if (! $completed) {
            return back()->withInput()
                ->withError(trans('user::messages.users.invalid_reset_code'));
        }
        return redirect($this->loginUrl())
            ->withSuccess(trans('user::messages.users.password_has_been_reset'));
    }

    public function verifyRegister(Request $request)
    {
        $user_id = $request->user_id;
        $code = $request->code;
        $user = User::where('id', $user_id)->first();
        if (is_null($user)) {
            return redirect($this->loginUrl())
                ->withError(trans('user::messages.users.no_user_found'));
        }
        $activation = Activation::completed($user);
        if($activation){
            return redirect($this->loginUrl())
                ->withError(trans('user::mail.user_have_verify'));
        }
        if($code != $user->verify_code){
            return redirect($this->loginUrl())
                ->withError(trans('user::mail.verify_not_correct'));
        }
        $activation = Activation::create($user);
        if (Activation::complete($user, $activation->code))
        {
            $user->status = '1';
            $user->save();
            // if (setting('welcome_email')) {
            //     Mail::to($user->email)
            //         ->send(new Welcome($user->first_name));
            // }

            return redirect($this->loginUrl())
            ->withSuccess(trans('user::mail.verify_success'));
        }
        else
        {
            return redirect($this->loginUrl())
                ->withError(trans('user::mail.verify_not_correct'));
        }
    }
}
