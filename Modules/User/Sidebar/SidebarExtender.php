<?php

namespace Modules\User\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.system'), function (Group $group) {
            $group->item(trans('user::sidebar.users'), function (Item $item) {
                $item->weight(5);
                $item->icon('fa fa-users');
                $item->route('admin.users.index');
                $item->authorize(
                    $this->auth->hasAccess('admin.users.index')
                );
            });
        });
        $menu->group(trans('admin::sidebar.system'), function (Group $group) {
            $group->item(trans('user::sidebar.curators'), function (Item $item) {
                $item->weight(5);
                $item->icon('fa fa-users');
                $item->route('admin.curators.index');
                $item->authorize(
                    $this->auth->hasAccess('admin.curators.index')
                );
            });
        });
    }
}
