<?php

use Illuminate\Support\Facades\Route;

Route::get('login', 'AuthController@getLogin')->name('login');
Route::post('login', 'AuthController@postLogin')->name('login.post');
Route::get('register', 'AuthController@getRegister')->name('register');
Route::post('register', 'AuthController@postRegister')->name('register.post');

Route::get('login/{provider}', 'AuthController@redirectToProvider')->name('login.redirect');
Route::get('login/{provider}/callback', 'AuthController@handleProviderCallback')->name('login.callback');

Route::get('logout', 'AuthController@getLogout')->name('logout');

Route::get('password/reset', 'AuthController@getReset')->name('reset');
Route::post('password/reset', 'AuthController@postReset')->name('reset.post');
Route::get('password/reset/{email}/{code}', 'AuthController@getResetComplete')->name('reset.complete');
Route::post('password/reset/{email}/{code}', 'AuthController@postResetComplete')->name('reset.complete.post');

Route::get('forgotpassword', 'AuthController@forgotPassword')->name('forgotpassword');
Route::get('changepassword', 'AuthController@changePassword')->name('changepassword');
Route::get('verifyemail', 'AuthController@verifyEmail')->name('verifyemail');
Route::post('verifyemail', 'AuthController@sendVerifyEmail')->name('verifyemail.post');
Route::get('register/verify', 'AuthController@verifyRegister')->name('register.verify');

Route::get('profile', 'UserController@profile')->name('profile');
