<?php

namespace Modules\User\Entities;

use Modules\Order\Entities\Order;
use Modules\Support\Eloquent\Model;
use Modules\Payment\Facades\Gateway;
use Modules\Transaction\Admin\TransactionTable;

class Transaction extends Model
{
    /**
     * Get the user of transaction
     */
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
