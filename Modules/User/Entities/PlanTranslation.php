<?php

namespace Modules\User\Entities;

use Modules\Support\Eloquent\Model;

class PlanTranslation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * Get the subscription plan of plan subscription
     */
    public function subscription_plan()
    {
        return $this->belongsTo(SubscriptionPlan::class, 'plan_id');
    }
}
