<?php

namespace Modules\User\Entities;

use Modules\Support\Eloquent\Model;

class SubscriptionPlan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [''];

    /**
     * 
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get the plan translation of subsciption plan
     */
    public function plan_translation()
    {
        return $this->hasOne(PlanTranslation::class, 'plan_id');
    }
}
