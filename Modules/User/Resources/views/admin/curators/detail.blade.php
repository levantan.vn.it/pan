@php $countries = Modules\Support\Country::all() @endphp
<div class="modal fade modal-update" id="examplemodal" tabindex="-1" role="dialog" aria-hidden="true"
  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content form">
      <form action="{{ route('admin.curators.update', $curator) }}" method="post" name="update"
        enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="modal-header">
          <div id="validate-message"> </div>
          <h5 class="modal-title">@lang('user::curators.details.title')</h5>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <p>@lang('user::curators.details.avatar')</p>
            <div class="avatar">
              <img src="{{ $curator->path_avatar }}" data-src="{{ $curator->path_avatar }}">
              <input type="file" name="avatar" id="avatar" accept="image/*" onchange="preAvatar(this)">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::curators.details.id')</label>
              <input type="text" class="form-control" value="{{ $curator->id }}" readonly>
            </div>
            <div class="form-group">
              <label for="">@lang('user::curators.details.name')</label>
              <input type="text" name="name" class="form-control" value="{{ $curator->name }}" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::curators.details.email')</label>
              <input type="email" name="email" class="form-control" value="{{ $curator->email }}" required>
            </div>
            <div class="form-group">
              <label for="">@lang('user::curators.details.country')</label>
              <select name="country_id" class="form-control">
                <option value="">None</option>
                @foreach ($countries as $code => $name)
                <option value="{{ $code }}" @if($curator->country_id == $code) selected @endif>
                  {{ $name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::curators.details.created')</label>
              <input type="date" class="form-control" value="{{ optional($curator->created_at)->format('Y-m-d') }}"
                readonly>
            </div>
            <div class="form-group">
              <label for="">@lang('user::curators.details.status')</label>
              <select name="status" class="form-control">
                <option value="0" @if($curator->status == 0) selected @endif>
                  @lang('user::curators.details.status_option.0')
                </option>
                <option value="1" @if($curator->status == 1) selected @endif>
                  @lang('user::curators.details.status_option.1')
                </option>
                <option value="2" @if($curator->status == 2) selected @endif>
                  @lang('user::curators.details.status_option.2')
                </option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::curators.details.collection')</label>
              <input readonly type="text" class="form-control" value="{{ $curator->collections }}">
            </div>
            <div class="form-group">
              <label for="">@lang('user::curators.details.liked')</label>
              <input readonly type="text" class="form-control" value="{{ $curator->likes }}">
            </div>
            <div class="form-group">
              <label for="">@lang('user::curators.details.followers')</label>
              <input readonly type="text" class="form-control" value="{{ $curator->followers }}">
            </div>
          </div>
          <div>
            <a href="javascript:;" class="recovery-psd">@lang('user::curators.details.recovery_psd_email')</a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">
            @lang('user::curators.details.button.cancel')
          </button>
          <button type="submit" class="btn btn-primary btn-save">
            @lang('user::curators.details.button.save')
          </button>
        </div>
      </form>
    </div>
  </div>
</div>