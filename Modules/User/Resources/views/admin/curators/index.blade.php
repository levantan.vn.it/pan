@extends('admin::layout')

@component('admin::components.page.header')
@slot('title', trans('user::curators.curators'))
<li class="active">{{ trans('user::curators.curators') }}</li>
@endcomponent
@push('styles')
<link rel="stylesheet" href="{{ asset('modules/user/admin/css/manager.css') }}">
@endpush

@component('user::components.index')
@slot('add', 'create')

@slot('resource', 'users')

@slot('name', trans('user::curators.curator'))

@section('search_panel')
<div class="form form-search">
  <form action="javascript:;" name="search">
    <div class="form-row">
      <div class="form-group">
        <label for="search_value">@lang('user::curators.panel.input_key')</label>
        <input type="text" name="search_value" class="form-control">
      </div>
      <div class="form-group">
        <label for="">@lang('user::curators.panel.status')</label>
        <select name="status" class="form-control">
          <option value="">@lang('user::curators.panel.status_option.all')</option>
          <option value="0">@lang('user::curators.panel.status_option.0')</option>
          <option value="1">@lang('user::curators.panel.status_option.1')</option>
          <option value="2">@lang('user::curators.panel.status_option.2')</option>
        </select>
      </div>
    </div>
    <div class="form-row form-row-end">
      <div class="form-group joined-date">
        <label for="">@lang('user::curators.panel.created')</label>
        <div class="form-row">
          <div class="col col-2 form-group">
            <input type="date" name="date_from" class="form-control" min="2020-01-01" max="{{ Date('Y-m-d') }}">
          </div>
          <div class="col col-2 form-group">
            <input type="date" name="date_to" class="form-control" min="2020-01-01" max="{{ Date('Y-m-d') }}">
          </div>
        </div>
      </div>
      <div class="form-row form-group follower-action">
        <div class="form-group follower">
          <label for="">@lang('user::curators.panel.followers')</label>
          <select name="followers" class="form-control">
            <option value="">@lang('user::curators.panel.followers_option.default')</option>
            <option value="asc">@lang('user::curators.panel.followers_option.asc')</option>
            <option value="desc">@lang('user::curators.panel.followers_option.desc')</option>
          </select>
        </div>
        <div class="form-group btn-action">
          <button type="button" class="btn btn-search">@lang('user::curators.panel.button.search')</button>
          <button type="reset" class="btn btn-reset">@lang('user::curators.panel.button.reset')</button>
          <button type="button" class="btn btn-excel">@lang('user::curators.panel.button.excel')</button>
        </div>
      </div>
    </div>
  </form>

</div>
@endsection
@slot('thead')
<tr>
  @include('admin::partials.table.select_all')
  <th>@lang('admin::admin.table.id')</th>
  <th>@lang('user::curators.table.avatar')</th>
  <th>@lang('user::curators.table.name')</th>
  <th>@lang('user::curators.table.email')</th>
  <th>@lang('user::curators.table.collection')</th>
  <th>@lang('user::curators.table.followers')</th>
  <th>@lang('user::curators.table.created')</th>
  <th>@lang('user::curators.table.status')</th>
  <th>@lang('user::curators.table.action')</th>
</tr>
@endslot
<div id="detail"></div>
@endcomponent

@push('scripts')
<script>
  let search = false;
    const datatable = new UserDatatable('#users-table .table', {
        searching: false,
        ordering: false,
        ajax: function(defaultParams, callback, settings) {
            const params = getParams(defaultParams);
            $.get(location.href + params).then(data => {
                callback(data);
            })
        },
        info: false,
        lengthChange: false,
        columns: [{
                data: 'checkbox',
                width: '3%'
            },
            {
                data: 'id',
                width: '5%'
            },
            {
                data: 'avatar'
            },
            {
                data: 'name'
            },
            {
                data: 'email',
            },
            {
                data: 'collections'
            },
            {
                data: 'followers'
            },
            {
                data: 'created',
                name: 'created_at'
            },
            {
                data: 'status'
            },
            {
                data: 'action'
            }
        ]
    });

    function getParams (params = {}, excel = false){
      const draws = excel ? {excel: true} : {
        draw: params.draw,
        length: params.length,
        start: params.start
      }

      return '?' +$.param({
        ...draws,
        search: search,
        search_value: document.forms.search.search_value.value,
        status: document.forms.search.status.value,
        followers: document.forms.search.followers.value,
        created_at: [
            document.forms.search.date_from.value ?? 0,
            document.forms.search.date_to.value ?? 1
        ]
      });
    }


    $(function() {
        let date_from = 'input[name="date_from"]';
        let date_to = 'input[name="date_to"]';
        let minDate = $(date_from).attr('min');
        let maxDate = $(date_from).attr('max');
        $(date_from).on('change', function(){
            $(date_to).attr('min', $(this).val());
        });

        $(date_to).on('change', function(){
            $(date_from).attr('max', $(this).val());
        });

        $('.btn-search').on('click', function() {
            search = true;
            datatable.element.api().ajax.reload();
        });

        $('button[type="reset"]').on('click', function() {
            search = false;
            $(date_from).attr({min: minDate, max: maxDate});
            $(date_to).attr({min: minDate, max: maxDate});
            datatable.element.api().ajax.reload();
        });

        $(document).on('click', '.btn-edit', function(e) {
            e.preventDefault();
            $.get($(this).attr('href')).then(data => {
                $('#detail').html(data);
                $('#detail .modal').modal('show');
            }).catch(err => {

            });
        });

        $(document).on('submit', `form[name='update']`, function(event){
            event.preventDefault();
            const data = new FormData(document.forms.update);
            $.ajax({
                url: $(this).attr('action'),
                method: 'POST',
                data: new FormData(document.forms.update),
                processData: false,
                contentType: false,
                success(data){
                    $('.modal.modal-update').modal('hide');
                    datatable.element.api().ajax.reload();
                },
                error(err){
                  if(err.status == 422){
                    const errr = err.responseJSON.errors;
                    let html = '';
                    for(const [k,v] of Object.entries(errr)){
                      html+=`<p>${v}</p>`
                    }
                    $('#validate-message').html(html)
                  }
                }
            })
        });

      $(document).on('change', `#user_type`, function(){
        if($(this).val() ==2 ){
          $('#business-info').addClass('is-business');
          $('#business-info input:required').attr('required', true);
        }else{
          $('#business-info').removeClass('is-business');
          $('#business-info input:required').attr('required', false);
        }
      });

      $(document).on('click', '.btn-create', function(e){
        e.preventDefault();
      })
    });
</script>
@endpush