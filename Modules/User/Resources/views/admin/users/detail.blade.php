@php $countries = Modules\Support\Country::all() @endphp
<div class="modal fade modal-update" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true"
  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content form">
      <form action="{{ route('admin.users.update', $user) }}" method="post" name="update" enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="modal-header">
          <div id="validate-message"> </div>
          <h5 class="modal-title">@lang('user::users.user_detail')</h5>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <p>@lang('user::users.avatar')</p>
            <div class="avatar">
              <img src="{{ $user->path_avatar }}" data-src="{{ $user->path_avatar }}">
              <input type="file" name="avatar" id="avatar" accept="image/*" onchange="preAvatar(this)">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::users.modal.user_id')</label>
              <input type="text" name="id" class="form-control" value="{{ $user->id }}" readonly>
            </div>
            <div class="form-group">
              <label for="">@lang('user::users.fullname')</label>
              <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::users.email')</label>
              <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
            </div>
            <div class="form-group">
              <label for="">@lang('user::users.user_types')</label>
              <select id="user_type" name="user_type" class="form-control" value="{{ $user->user_type }}" required>
                <option value="1" @if($user->user_type == 1) selected @endif>
                  @lang('user::users.user_type.1')
                </option>
                <option value="2" @if($user->user_type == 2) selected @endif>
                  @lang('user::users.user_type.2')
                </option>
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::users.modal.date_of_birth')</label>
              <input type="date" name="birthday" class="form-control" value="{{ $user->birthday }}">
            </div>
            <div class="form-group">
              <label for="">@lang('user::users.modal.country')</label>
              <select name="country_id" class="form-control">
                <option value="">None</option>
                @foreach ($countries as $code => $name)
                <option value="{{ $code }}" @if($user->country_id == $code) selected @endif>
                  {{ $name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::users.user_plan')</label>
              <select name="plan_id" class="form-control">
                <option value="">None</option>
                @foreach ($plans as $pl)
                <option value="{{ $pl->id }}" @if($pl->id == $user->plan_id) selected @endif>
                  {{ optional($pl->plan_translation)->name }}
                </option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="">@lang('user::users.modal.user_status')</label>
              <select name="status" class="form-control">
                <option value="0" @if($user->status == 0) selected @endif>
                  @lang('user::users.status.0')
                </option>
                <option value="1" @if($user->status == 1) selected @endif>
                  @lang('user::users.status.1')
                </option>
                <option value="2" @if($user->status == 2) selected @endif>
                  @lang('user::users.status.2')
                </option>
              </select>
            </div>
          </div>
          @php $business = optional($business ?? null) @endphp
          @php $isBusiness = $user->user_type == 2 @endphp
          <div id="business-info" class="@if($isBusiness) is-business @endif">
            <div class="business-info">
              <h5>@lang('user::users.business_info')</h5>
            </div>
            <div>
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.registration')</label>
                <div class="registration">
                  <div class="registration-item">
                    <img src="" alt="" width="140">
                    <input type="file" accept="image/*" onchange="preAvatar(this)">
                    <button class="registration-close" type="button"></button>
                  </div>
                  <div class="registration-item">
                    <img src="" alt="" width="140">
                    <input type="file" accept="image/*" onchange="preAvatar(this)">
                    <button class="registration-close" type="button"></button>
                  </div>
                  <div class="registration-item">
                    <img src="" alt="" width="140">
                    <input type="file" accept="image/*" onchange="preAvatar(this)">
                    <button class="registration-close" type="button"></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.name')</label>
                <input type="text" name="business_name" class="form-control" value="{{ $business->business_name }}"
                  required="{{ $isBusiness }}">
              </div>
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.type')</label>
                <select name="business_type" class="form-control">
                  <option value="0">None</option>
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.representative')</label>
                <input type="text" name="representative" class="form-control" value="{{ $business->representative }}"
                  required="{{ $isBusiness }}" maxlength="191">
              </div>
              <div class="form-group">
                <label for="">@lang('user::users.email')</label>
                <input type="email" name="business_email" class="form-control" value="{{ $business->business_email }}"
                  required="{{ $isBusiness }}" maxlength="191">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.address')</label>
                <input type="text" name="address" class="form-control" value="{{ $business->address }}"
                  required="{{ $isBusiness }}" maxlength="191">
              </div>
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.hotline')</label>
                <input type="tel" name="hotline" class="form-control" value="{{ $business->hotline }}"
                  required="{{ $isBusiness }}" maxlength="20" minlength="6">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.store_name')</label>
                <input type="text" name="store_name" class="form-control" value="{{ $business->store_name }}"
                  required="{{ $isBusiness }}" maxlength="191">
              </div>
              <div class="form-group">
                <label for="">@lang('user::users.modal.business.store_addr')</label>
                <input type="text" name="store_address" class="form-control" value="{{ $business->store_address }}"
                  required="{{ $isBusiness }}" maxlength="191">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="">@lang('user::users.playlists')</label>
              <input readonly type="text" class="form-control" value="{{ $user->playlists }}">
            </div>
            <div class="form-group">
              <label for="">@lang('user::users.modal.liked_number')</label>
              <input readonly type="text" class="form-control" value="{{ $user->likes }}">
            </div>
            <div class="form-group">
              <label for="">@lang('user::users.followers')</label>
              <input readonly type="text" class="form-control" value="{{ $user->followers }}">
            </div>
          </div>
          <div>
            <details>
              <summary>
                <span>@lang('user::users.modal.transactions_history')</span>
              </summary>
              <table class="table table-borderless">
                <thead>
                  <th>@lang('user::users.modal.transactions.id')</th>
                  <th>@lang('user::users.modal.transactions.date')</th>
                  <th>@lang('user::users.modal.transactions.value')</th>
                  <th>@lang('user::users.status_s')</th>
                </thead>
                <tbody>
                  @foreach($transactions as $transaction)
                  <tr>
                    <td>{{ $transaction->id }}</td>
                    <td>{{ $transaction->paid_date }}</td>
                    <td>{{ $transaction->amount }}</td>
                    <td>{{ $transaction->status }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </details>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">
            @lang('user::users.modal.btn_cancel')
          </button>
          <button type="submit" class="btn btn-primary btn-save">
            @lang('user::users.modal.btn_save')
          </button>
        </div>
      </form>
    </div>
  </div>
</div>