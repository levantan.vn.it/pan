@extends('admin::layout')

@component('admin::components.page.header')
@slot('title', trans('user::users.users'))
<li class="active">{{ trans('user::users.users') }}</li>
@endcomponent
@push('styles')
<link rel="stylesheet" href="{{ asset('modules/user/admin/css/manager.css') }}">
@endpush

@component('user::components.index')

@slot('resource', 'users')

@slot('name', trans('user::users.user'))

@section('search_panel')
<div class="form form-search">
  <form action="javascript:;" name="search">
    <div class="form-row">
      <div class="form-group">
        <label for="search_value">User Email/Name/ID</label>
        <input type="text" name="search_value" class="form-control">
      </div>
      <div class="form-row flex-grow">
        <div class="form-group">
          <label for="">Status</label>
          <select name="status" class="form-control">
            <option value="">All</option>
            <option value="0">{{ __('user::users.status.0') }}</option>
            <option value="1">{{ __('user::users.status.1') }}</option>
            <option value="2">{{ __('user::users.status.2') }}</option>
          </select>
        </div>
        <div class="form-group">
          <label for="">User Plan</label>
          <select name="plan_id" class="form-control">
            <option value="">All</option>
            @foreach ($plans as $plan)
            <option value="{{ $plan->id }}">{{ $plan->plan_translation->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="">User Type</label>
          <select name="user_type" class="form-control">
            <option value="">All</option>
            <option value="1">{{ __('user::users.user_type.1') }}</option>
            <option value="2">{{ __('user::users.user_type.2') }}</option>
          </select>
        </div>
      </div>
    </div>
    <div class="form-row form-row-end">
      <div class="form-group joined-date">
        <label for="">Joined date</label>
        <div class="form-row">
          <div class="col col-2 form-group">
            <input type="text" onfocus="(this.type='date')" onfocusout="(this.type='text')" name="date_from"
              class="form-control" min="2020-01-01" max="{{ Date('Y-m-d') }}" placeholder="From">
          </div>
          <div class="col col-2 form-group">
            <input type="text" name="date_to" class="form-control" min="2020-01-01" max="{{ Date('Y-m-d') }}"
              onfocus="(this.type='date')" onfocusout="(this.type='text')" placeholder="To">
          </div>
        </div>
      </div>
      <div class="form-row form-group follower-action">
        <div class="form-group follower">
          <label for="">Followers</label>
          <select name="followers" class="form-control">
            <option value="">Select order</option>
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
        </div>
        <div class="form-group btn-action">
          <button type="button" class="btn btn-search">Search</button>
          <button type="reset" class="btn btn-reset">Reset</button>
          <button type="button" class="btn btn-excel">Excel</button>
        </div>
      </div>
    </div>
  </form>

</div>
@endsection
@slot('thead')
<tr>
  @include('admin::partials.table.select_all')
  <th>@lang('admin::admin.table.id')</th>
  <th>@lang('user::users.table.avatar')</th>
  <th>@lang('user::users.table.fullname')</th>
  <th>@lang('user::users.table.email')</th>
  <th>@lang('user::users.table.user_type')</th>
  <th>@lang('user::users.table.playlists')</th>
  <th>@lang('user::users.table.user_plan')</th>
  <th>@lang('user::users.table.followers')</th>
  <th>@lang('user::users.table.joined_date')</th>
  <th>@lang('user::users.table.status')</th>
  <th>@lang('user::users.table.action')</th>
  <!-- <th data-sort>{{ trans('admin::admin.table.created') }}</th> -->
</tr>
@endslot
<div id="detail"></div>
@endcomponent

@push('scripts')
<script>
  let search = false;
  const datatable = new UserDatatable('#users-table .table', {
        searching: false,
        ordering: false,
        ajax: function(params, callback, settings) {

            $.get(location.href + getParams(params)).then(data => {
                callback(data);
            })
        },
        info: false,
        lengthChange: false,
        columns: [{
                data: 'checkbox',
                width: '3%'
            },
            {
                data: 'id',
                width: '5%'
            },
            {
                data: 'avatar'
            },
            {
                data: 'name'
            },
            {
                data: 'email',
            },
            {
                data: 'user_type'
            },
            {
                data: 'playlists'
            },
            {
                data: 'user_plan'
            },
            {
                data: 'followers'
            },
            {
                data: 'joined_date',
                name: 'created_at'
            },
            {
                data: 'status'
            },
            {
                data: 'action'
            }
        ]
    });

    function getParams (params = {}, excel = false){
      const draws = excel ? {excel: true} : {
        draw: params.draw,
        length: params.length,
        start: params.start
      }

      return '?' +$.param({
        ...draws,
        search: search,
        search_value: document.forms.search.search_value.value,
        status: document.forms.search.status.value,
        user_type: document.forms.search.user_type.value,
        followers: document.forms.search.followers.value,
        plan_id: document.forms.search.plan_id.value,
        created_at: [
            document.forms.search.date_from.value ?? 0,
            document.forms.search.date_to.value ?? 1
        ]
      });
    }



    $(function() {
        $(document).on('click', 'input[name="date_form"], input[name="date_to"]', function(){
          $(this).attr('type', 'date');
        });
        $(document).on('mousemove, focus', 'input[name="date_form"], input[name="date_to"]', function(){
          $(this).attr('type', 'text');
        });
        let date_from = 'input[name="date_from"]';
        let date_to = 'input[name="date_to"]';
        let minDate = $(date_from).attr('min');
        let maxDate = $(date_from).attr('max');
        $(date_from).on('change', function(){
            $(date_to).attr('min', $(this).val());
        });

        $(date_to).on('change', function(){
            $(date_from).attr('max', $(this).val());
        });

        $('.btn-search').on('click', function() {
            search = true;
            datatable.element.api().ajax.reload();
        });

        $('button[type="reset"]').on('click', function() {
            search = false;
            $(date_from).attr({min: minDate, max: maxDate});
            $(date_to).attr({min: minDate, max: maxDate});
            datatable.element.api().ajax.reload();
        });

        $(document).on('click', '.btn-edit', function(e) {
            e.preventDefault();
            $.get($(this).attr('href')).then(data => {
                $('#detail').html(data);
                $('#detail .modal').modal('show');
            }).catch(err => {

            });
        });

        $(document).on('submit', `form[name='update']`, function(event){
            event.preventDefault();
            const data = new FormData(document.forms.update);
            $.ajax({
                url: $(this).attr('action'),
                method: 'POST',
                data: new FormData(document.forms.update),
                processData: false,
                contentType: false,
                success(data){
                    $('.modal.modal-update').modal('hide');
                    datatable.element.api().ajax.reload();
                },
                error(err){
                  if(err.status == 422){
                    const errr = err.responseJSON.errors;
                    let html = '';
                    for(const [k,v] of Object.entries(errr)){
                      html+=`<p>${v}</p>`
                    }
                    $('#validate-message').html(html)
                  }
                }
            })
        });

      $(document).on('change', `#user_type`, function(){
        if($(this).val() ==2 ){
          $('#business-info').addClass('is-business');
          $('#business-info input:required').attr('required', true);
        }else{
          $('#business-info').removeClass('is-business');
          $('#business-info input:required').attr('required', false);
        }
      });
    });

    function preAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $(input).parent().find('img').attr('src', e.target.result);
                $(input).attr('hidden', true);
                $(input).parent().find('button').click(function(){
                    $(input).parent().find('img').attr('src', '');
                    $(input).val();
                    $(input).attr('hidden', false);
                })
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }else{
            $(input).parent().find('img').attr('src', $(img).data('src'));
        }
    }
</script>
@endpush