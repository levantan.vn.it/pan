@extends('admin::excel_layout')
@section('thead')
<th>@lang('admin::admin.table.id')</th>
<th>@lang('user::users.table.avatar')</th>
<th>@lang('user::users.table.fullname')</th>
<th>@lang('user::users.table.email')</th>
<th>@lang('user::users.table.user_type')</th>
<th>@lang('user::users.table.playlists')</th>
<th>@lang('user::users.table.user_plan')</th>
<th>@lang('user::users.table.followers')</th>
<th>@lang('user::users.table.joined_date')</th>
<th>@lang('user::users.table.status')</th>
@endsection
@section('tbody')
@foreach ($data as $item)
<tr>
    <td>@lang('user::users.user_id',['id'=>$item->id])</td>
    <td>{{ $item->path_avatar }}</td>
    <td>{{ $item->name }}</td>
    <td>{{ $item->email }}</td>
    <td>{{ $item->user_type }}</td>
    <td>{{ $item->playlists }}</td>
    <td>{{ $item->plan_id }}</td>
    <td>{{ $item->followers }}</td>
    <td>{{ $item->created_at->format('d M') }}</td>
    <td>
        <strong>@lang('user::users.status.'. $item->status)</strong>
    </td>
</tr>
@endforeach
@endsection