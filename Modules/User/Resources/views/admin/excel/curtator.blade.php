@extends('admin::excel_layout')
@section('thead')
<td>@lang('admin::admin.table.id')</td>
<td>@lang('user::curators.table.avatar')</td>
<td>@lang('user::curators.table.name')</td>
<td>@lang('user::curators.table.email')</td>
<td>@lang('user::curators.table.collection')</td>
<td>@lang('user::curators.table.followers')</td>
<td>@lang('user::curators.table.created')</td>
<td>@lang('user::curators.table.status')</td>
@endsection
@section('tbody')
@foreach ($data as $item)
<tr>
    <td>@lang('user::curators.table.id'){{ $item->id }}</td>
    <td>{{ $item->path_avatar }}</td>
    <td>{{ $item->name }}</td>
    <td>{{ $item->email }}</td>
    <td>{{ $item->collections }}</td>
    <td>{{ $item->followers }}</td>
    <td>{{ $item->created_at->format('d M') }}</td>
    <td>
        <strong>@lang('user::curators.panel.status_option.'. $item->status)</strong>
    </td>
</tr>
@endforeach
@endsection