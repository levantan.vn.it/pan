<div style="width: 50px; height: 50px; border-radius: 50%; overflow: hidden;">
    <img src="{{ $avatar }}" alt="Avatar" width="50px" height="50px" style="object-fit: cover;">
</div>