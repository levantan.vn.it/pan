@section('content')
<div class="row">
    <div class="btn-group pull-right">
        @if (isset($buttons, $name))
        @foreach ($buttons as $view)
        <a href="{{ route("admin.{$resource}.{$view}") }}" class="btn btn-primary btn-actions btn-{{ $view }}">
            {{ trans("admin::resource.{$view}", ['resource' => $name]) }}
        </a>
        @endforeach
        @else
        {{ $buttons ?? '' }}
        @endif
        @if (isset($add))
        <a href="{{ route("admin.{$resource}.{$add}") }}" class="btn btn-primary btn-actions btn-{{ $add }}"
            style="background-color: #18a0fb;">
            @lang('admin::resource.add')
        </a>
        @endif
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
        @yield('search_panel')
    </div>
</div>

<div class="box box-primary">
    <div class="box-body index-table" id="{{ isset($resource) ? "{$resource}-table" : '' }}">
        @if (isset($thead))
        @include('admin::components.table')
        @else
        {{ $slot }}
        @endif
    </div>
</div>
@endsection

@isset($name)
@push('shortcuts')
@if (isset($buttons) && in_array('create', $buttons))
<dl class="dl-horizontal">
    <dt><code>c</code></dt>
    <dd>{{ trans('admin::resource.create', ['resource' => $name]) }}</dd>
</dl>
@endif

<dl class="dl-horizontal">
    <dt><code>Del</code></dt>
    <dd>{{ trans('admin::resource.delete', ['resource' => $name]) }}</dd>
</dl>
@endpush

@push('scripts')
<script>
    @if (isset($buttons) && in_array('create', $buttons))
                keypressAction([
                    { key: 'c', route: '{{ route("admin.{$resource}.create") }}'}
                ]);
            @endif

            Mousetrap.bind('del', function () {
                $('{{ $selector ?? '' }} .btn-delete').trigger('click');
            });

            @isset($resource)
                DataTable.setRoutes('#{{ $resource }}-table .table', {
                    index: '{{ "admin.{$resource}.index" }}',
                    edit: '{{ "admin.{$resource}.edit" }}',
                    destroy: '{{ "admin.{$resource}.destroy" }}',
                });
            @endisset
    
  class UserDatatable extends DataTable {
        onRowClick(handler) {
            let row = 'tbody tr.clickable-row td';
            if (this.element.find('.select-all').length !== 0) {
                row += ':not(:first-child)';
            }
        }
    }
    $(function(){        
        $(document).on('click', '.btn-excel', function(){
            const url = location.href + getParams({}, true);
            const a = document.createElement('a');
            a.style.display = 'none';
            a.href = url;
            // the filename you want
            a.download = 'true';
            document.body.appendChild(a);
            a.click();
            a.remove();
        });
    })

    function preAvatar(input) {
        if (input.files && input.files[0]) {                
        $(input).parent().find('img').attr('src', URL.createObjectURL(input.files[0]));
        $(input).attr('hidden', true);
        $(input).parent().find('button').click(function(){
            $(input).parent().find('img').attr('src', '');
            $(input).val();
            $(input).attr('hidden', false);
        })
        }else{
            $(input).parent().find('img').attr('src', $(img).data('src'));
        }
    }
</script>
@endpush
@endisset