<?php

return [
    'user' => 'User',
    'users' => 'Users',
    'profile' => 'Profile',
    'user_id' => '@UserID:id',
    'email' => 'Email',
    'status_s' => 'Status',
    'avatar' => 'Avatar',
    'fullname' => 'Full name',
    'user_plan' => 'User Plan',
    'playlists' => 'Playlist',
    'followers' => 'Followers',
    'user_detail' => 'User Detail',
    'business_info' => 'Business Profile',
    'modal' => [
        'btn_cancel' => 'Cancel',
        'btn_save' => 'Save',
        'user_id' => 'User ID',
        'date_of_birth' => 'Date of Birth',
        'country' => 'Country',
        'user_status' => 'User status',
        'liked_number' => 'Liked Number',
        'transactions_history' => 'Transactions history',
        'transactions' => [
            'id' => 'ID',
            'date' => 'Date',
            'value' => 'Value',
        ],
        'business_profile' => 'Business Profile',
        'business' => [
            'registration' => 'Business Registration',
            'name' => 'Business Name',
            'type' => 'Business Type',
            'representative' => 'Representative',
            'address' => 'Business Address',
            'hotline' => 'Hotline',
            'store_name' => 'Store Name',
            'store_addr' => 'Store Address'
        ]
    ],
    'table' => [
        'id' => 'ID',
        'avatar' => 'Avatar',
        'fullname' => 'Full name',
        'email' => 'Email',
        'user_type' => 'User type',
        'playlists' => 'Playlist',
        'user_plan' => 'User Plan',
        'followers' => 'Followers',
        'joined_date' => 'Joined date',
        'status' => 'Status',
        'action' => 'Action',
    ],
    'status' => [
        '0' => 'Unverity',
        '1' => 'Active',
        '2' => 'Blocked'
    ],
    'user_types' => 'User type',
    'user_type' => [
        '1' => 'Normal',
        '2' => 'Business'
    ],
    'tabs' => [
        'group' => [
            'user_information' => 'User Information',
            'profile_information' => 'Profile Information',
        ],
        'account' => 'Account',
        'permissions' => 'Permissions',
        'new_password' => 'New Password',
    ],
    'form' => [
        'activated' => 'Activated',
    ],
    'or_reset_password' => 'or, Reset Password',
    'send_reset_password_email' => 'Send Reset Password Email',
];
