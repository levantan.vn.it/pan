<?php

return [
    'curator' => 'Curator',
    'curators' => 'Curators',
    'curator_detail' => 'Curator Detail',
    'panel' => [
        'input_key' => 'Curator Email / Name / ID',
        'status' => 'Status',
        'status_option' => [
            'all' => 'All',
            '0' => 'Unverity',
            '1' => 'Active',
            '2' => 'Blocked'
        ],
        'created' => 'Created date',
        'followers' => 'Followers',
        'followers_option' => [
            'default' => 'Select order',
            'asc' => 'Ascending',
            'desc' => 'Descending'
        ],
        'button' => [
            'search' => 'Search',
            'reset' => 'Reset',
            'excel' => 'Excel'
        ]
    ],
    'table' => [
        'id' => '@CuratorID',
        'avatar' => 'Avatar',
        'name' => 'Curator Name',
        'email' => 'Email',
        'collection' => 'Collection',
        'followers' => 'Followers',
        'created' => 'Created date',
        'status' => 'Status',
        'action' => 'Action'
    ],
    'details' => [
        'title' => 'Curator Detail',
        'id' => 'Curator ID',
        'avatar' => 'Avatar',
        'name' => 'Curator Name',
        'email' => 'Email',
        'country' => 'Country',
        'created' => 'Created date',
        'status' => 'Curator Status',
        'status_option' => [
            '0' => 'Unverity',
            '1' => 'Active',
            '2' => 'Blocked'
        ],
        'collection' => 'Collection',
        'liked' => 'Liked Number',
        'followers' => 'Followers',
        'recovery_psd_email' => 'Send recovery password email',
        'button' => [
            'cancel' => 'Cancel',
            'save' => 'Save'
        ]
    ]
];
