<?php

namespace Modules\User\Admin;

use Modules\Admin\Ui\AdminExport;
use Modules\Admin\Ui\AdminTable;
use Modules\User\Entities\PlanTranslation;

class UserExport extends AdminExport
{
    protected $view = 'user::admin.excel.user';
    public $filename = 'Users';
}
