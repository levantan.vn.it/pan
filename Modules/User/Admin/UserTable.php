<?php

namespace Modules\User\Admin;

use Modules\Admin\Ui\AdminTable;
use Modules\User\Entities\PlanTranslation;

class UserTable extends AdminTable
{
    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $rawColumns = ['last_login'];

    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable()
            ->editColumn('avatar', function ($user) {
                if ($user->avatar) {
                    $avatar = $user->path_avatar;
                } else {
                    $avatar = asset('media/default.jpg');
                }
                return view('user::admin.partials.table.avatar', compact('avatar'));
            })
            ->editColumn('status', function ($user) {
                return view('user::admin.partials.table.status', ['status' => $user->status]);
            })
            ->editColumn('user_plan', function ($user) {
                $plan = PlanTranslation::where('plan_id', $user->plan_id)->first();
                return optional($plan)->name;
            })
            ->editColumn('joined_date', function ($user) {
                return $user->created_at->format('d M');
            })
            ->editColumn('user_type', function ($user) {
                return __('user::users.user_type.' . $user->user_type);
            })
            ->editColumn('id', function ($user) {
                return __('user::users.user_id', ['id' => $user->id]);
            })
            ->editColumn('action', function ($user) {
                return view('user::admin.users.partials.action', ['id' => $user->id]);
            });
    }
}
