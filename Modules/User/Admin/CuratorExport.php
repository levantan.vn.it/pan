<?php

namespace Modules\User\Admin;

use Modules\Admin\Ui\AdminExport;
use Modules\Admin\Ui\AdminTable;
use Modules\User\Entities\PlanTranslation;

class CuratorExport extends AdminExport
{
    protected $view = 'user::admin.excel.curtator';
    public $filename = 'Curators';
}
