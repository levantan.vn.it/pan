<?php

namespace Modules\User\Admin;

use Modules\Admin\Ui\AdminTable;
use Modules\User\Entities\PlanTranslation;

class CuratorTable extends AdminTable
{
    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable()
            ->editColumn('avatar', function ($user) {
                if ($user->avatar) {
                    $avatar = $user->path_avatar;
                } else {
                    $avatar = asset('media/default.jpg');
                }
                return view('user::admin.partials.table.avatar', compact('avatar'));
            })
            ->editColumn('status', function ($user) {
                return view('user::admin.partials.table.status', ['status' => $user->status]);
            })
            ->editColumn('created', function ($user) {
                return $user->created_at->format('d M');
            })
            ->editColumn('action', function ($user) {
                return view('user::admin.curators.partials.action', ['id' => $user->id]);
            })
            ->editColumn('id', function ($user) {
                return trans('user::curators.table.id') . $user->id;
            });
    }
}
