<?php

namespace Modules\Admin\Ui;

use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\View\View;
use Maatwebsite\Excel\Excel;

class AdminExport implements FromView
{
    use Exportable;

    protected $view = 'admin::excel_layout';
    protected $data;
    public $filename = 'excel';
    public $writerType = Excel::XLSX;

    public function __construct(Builder $query)
    {
        $this->data = $query->get();
    }

    public function view(): View
    {
        return view($this->view, ['data' => $this->data]);
    }

    public static function excel(Builder $query)
    {
        $excel = new static($query);
        $filename = $excel->filename . '.' . $excel->writerType;
        return $excel->download($filename, $excel->writerType);
    }
}
