<?php

return [
    'create' => 'Create :resource',
    'show' => 'Show :resource',
    'edit' => 'Edit :resource',
    'delete' => 'Delete :resource',
    'add' => '&#9547; Add'
];
