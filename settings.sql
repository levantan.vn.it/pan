-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 13, 2021 at 11:30 AM
-- Server version: 8.0.19
-- PHP Version: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pan`
--

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `is_translatable`, `plain_value`, `created_at`, `updated_at`) VALUES
(1, 'store_name', 1, NULL, '2020-10-15 09:46:20', '2020-10-15 09:46:20'),
(2, 'store_email', 0, 's:20:\"admin@fleetcart.test\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(3, 'search_engine', 0, 's:5:\"mysql\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(4, 'algolia_app_id', 0, 'N;', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(5, 'algolia_secret', 0, 'N;', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(6, 'active_theme', 0, 's:10:\"Storefront\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(7, 'supported_countries', 0, 'a:1:{i:0;s:2:\"BD\";}', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(8, 'default_country', 0, 's:2:\"BD\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(9, 'supported_locales', 0, 'a:1:{i:0;s:2:\"en\";}', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(10, 'default_locale', 0, 's:2:\"en\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(11, 'default_timezone', 0, 's:10:\"Asia/Dhaka\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(12, 'customer_role', 0, 'i:2;', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(15, 'cookie_bar_enabled', 0, 'b:1;', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(16, 'supported_currencies', 0, 'a:1:{i:0;s:3:\"USD\";}', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(17, 'default_currency', 0, 's:3:\"USD\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(19, 'newsletter_enabled', 0, 'b:0;', '2020-10-15 09:46:21', '2020-10-15 09:46:21'),
(39, 'storefront_copyright_text', 0, 's:92:\"Copyright © <a href=\"{{ store_url }}\">{{ store_name }}</a> {{ year }}. All rights reserved.\";', '2020-10-15 09:46:21', '2020-10-15 09:46:21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
